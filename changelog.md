### Version 1.1.0
- Removed limitations on number of methods in
[`h07.TemplateTests`](src/test/java/h07/TemplateTests.java) and fixed documentation.
- Changed CI/CD configuration to build documentation on demand (when pushing to master) instead of
deploying pre-built files. For this purpose, a task to retrieve all dependencies was added to
[`build.gradle.kts`](build.gradle.kts).
- Refactored utility classes:
  - replaced XML wrapper classes with pure DOM and XPath
  - removed hash checks when no update is available
  - rewrote some methods to use a File object instead of the file name as string
  - split Utils between TestClass and Assertions
- Added more documentation

### Version 1.0.3
- Fixed name of class in DirectedGraphImplTest.newInstance(...)'s error message.
- Fixed PathImplTest.newInstance(...)'s error message.

### Version 1.0.2
- Fixed bug where tests for the Dijkstra algorithm would interpret an empty map as correct (see !1).
Thank you very much, Osh!

### Version 1.0.1
- Removed entries for `package-info.java` files in `.test_metadata.xml`.

### Version 1.0.0
Initial version
