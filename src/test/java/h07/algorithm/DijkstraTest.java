package h07.algorithm;

import h07.TemplateTests;
import h07.algebra.IntegerAdditionTest;
import h07.graph.DirectedGraphImplTest;
import h07.graph.PathImplTest;
import h07.util.TestClass;
import h07.util.provider.DijkstraProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Map;

import static h07.util.Assertions.*;
import static java.lang.reflect.Modifier.*;

/**
 * Tests for class {@code Dijkstra}. <br><br>
 * Depends on {@link TemplateTests.ShortestPathsAlgorithmTemplateTest}, {@link DirectedGraphImplTest} and
 * {@link IntegerAdditionTest}.
 */
public class DijkstraTest extends TestClass {

    /** {@code Method} object for {@code ShortestPathsAlgorithm.shortestPaths(DirectedGraph, V, Monoid, Comparator)} */
    public final Method shortestPaths;

    /** Instance of {@link TemplateTests.ShortestPathsAlgorithmTemplateTest} */
    private final TemplateTests.ShortestPathsAlgorithmTemplateTest shortestPathsAlgorithm;
    /** Instance of {@link DirectedGraphImplTest} */
    private final DirectedGraphImplTest directedGraph;
    /** Instance of {@link IntegerAdditionTest} */
    private final IntegerAdditionTest integerAddition;

    /** Array of city names to use as nodes */
    private static final String[] NODES = {"München", "Augsburg", "Karlsruhe", "Erfurt", "Nürnberg", "Kassel",
            "Würzburg", "Stuttgart", "Mannheim", "Frankfurt"};
    /** Matrix of weights between nodes */
    private static final Integer[][] ADJACENCY_MATRIX = {
            {null,   84, null, null,  167,  502, null, null, null, null},
            {  84, null,  250, null, null, null, null, null, null, null},
            {null,  250, null, null, null, null, null, null,   80, null},
            {null, null, null, null, null, null,  186, null, null, null},
            { 167, null, null, null, null, null,  103,  183, null, null},
            { 502, null, null, null, null, null, null, null, null,  173},
            {null, null, null,  186,  103, null, null, null, null,  217},
            {null, null, null, null,  183, null, null, null, null, null},
            {null, null,   80, null, null, null, null, null, null,   85},
            {null, null, null, null, null,  173,  217, null,   85, null}
    };

    /**
     * Creates a new {@link DijkstraTest} object. Requires that {@code Dijkstra} exists
     * and has a constructor with no parameters.
     */
    public DijkstraTest() {
        super("h07.algorithm.Dijkstra", constructor -> constructor.getParameterCount() == 0);

        shortestPathsAlgorithm = TemplateTests.INSTANCE.new ShortestPathsAlgorithmTemplateTest();
        directedGraph = new DirectedGraphImplTest();
        integerAddition = new IntegerAdditionTest();

        shortestPaths = shortestPathsAlgorithm.shortestPaths;
    }

    /**
     * Tests the definition of {@code Dijkstra}.
     * <ul>
     *     <li>Asserts that the class...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is public</li>
     *         <li>is not abstract</li>
     *         <li>is generic and has the type parameters V and A</li>
     *         <li>implements {@link ShortestPathsAlgorithm}</li>
     *     </ul></li>
     * </ul>
     */
    @Test
    @Override
    public void testDefinition() {
        assertHasModifiers(classDescriptor, PUBLIC);
        assertDoesNotHaveModifiers(classDescriptor, ABSTRACT);
        assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);
        assertImplements(classDescriptor, shortestPathsAlgorithm.className + "<V, A>");
    }

    /**
     * Tests for {@code Dijkstra.shortestPaths(DirectedGraph, V, Monoid, Comparator)}. <br>
     * Asserts that the path returned by the tested method is actually the shortest from {@code startNode}
     * to any other node. <br>
     * Many thanks to Oshgnacknak for
     * <a href="https://git.rwth-aachen.de/aud-tests/AuD-2021-H07-Student/-/merge_requests/1">fixing a bug</a>
     * that occurred when the map returned by the tested method was empty.
     * @param startNode the starting node
     * @param paths     a map of all nodes and the shortest path to reach them from {@code startNode}
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(DijkstraProvider.class)
    public void testShortestPaths(String startNode, Map<String, String> paths) throws ReflectiveOperationException {
        //noinspection unchecked
        Map<String, Object> actualPaths = (Map<String, Object>) shortestPaths.invoke(
                newInstance(),
                directedGraph.newInstance(NODES, ADJACENCY_MATRIX),
                startNode,
                integerAddition.newInstance(),
                Comparator.naturalOrder());

        assertEquals(paths.keySet(), actualPaths.keySet(), "Returned key set of reachable nodes does not match expected one");
        for (Map.Entry<String, Object> entry : actualPaths.entrySet())
            assertEquals(paths.get(entry.getKey()), pathToString(entry.getValue()), "Returned shortest path did not match expected one");
    }

    /**
     * Returns the string representation of a given path. <br>
     * Requires {@link PathImplTest} and {@link PathImplTest.TraverserTest}.
     * @param path the path
     * @return the string representation of a given path
     * @throws ReflectiveOperationException if any invocation fails
     */
    private static String pathToString(Object path) throws ReflectiveOperationException {
        PathImplTest _path = new PathImplTest();
        PathImplTest.TraverserTest _traverser = _path.new TraverserTest();
        Object traverser = _path.traverser.invoke(path);
        StringBuilder builder = new StringBuilder(_traverser.getCurrentNode.invoke(traverser).toString());

        while ((Boolean) _traverser.hasNextNode.invoke(traverser)) {
            builder.append(" -").append(_traverser.getDistanceToNextNode.invoke(traverser)).append("-> ");

            _traverser.walkToNextNode.invoke(traverser);

            builder.append(_traverser.getCurrentNode.invoke(traverser));
        }

        return builder.toString();
    }
}
