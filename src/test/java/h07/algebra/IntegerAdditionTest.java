package h07.algebra;

import h07.TemplateTests;
import h07.util.TestClass;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;

import static h07.util.Assertions.*;
import static java.lang.reflect.Modifier.*;

/**
 * Tests for class {@code IntegerAddition}. <br><br>
 * Depends on {@link TemplateTests.MonoidTemplateTest}.
 */
public class IntegerAdditionTest extends TestClass {

    /** {@code Method} object for {@code Monoid.zero()} */
    public final Method zero;
    /** {@code Method} object for {@code Monoid.add(M, M)} */
    public final Method add;

    /** Instance of {@link TemplateTests.MonoidTemplateTest} */
    private final TemplateTests.MonoidTemplateTest monoid;

    /**
     * Creates a new {@link IntegerAdditionTest} object. Requires that {@code IntegerAddition} exists
     * and has a constructor with no parameters.
     */
    public IntegerAdditionTest() {
        super("h07.algebra.IntegerAddition", constructor -> constructor.getParameterCount() == 0);

        monoid = TemplateTests.INSTANCE.new MonoidTemplateTest();

        zero = monoid.zero;
        add = monoid.add;
    }

    /**
     * Tests the definition of {@code IntegerAddition}.
     * <ul>
     *     <li>Asserts that the class...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is public</li>
     *         <li>is not abstract</li>
     *         <li>implements {@link Monoid}</li>
     *     </ul></li>
     *     <li>Asserts that the constructor...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is public</li>
     *         <li>is the only constructor of this class</li>
     *     </ul></li>
     *     <li>Asserts that the class is stateless (no fields)</li>
     * </ul>
     */
    @Test
    @Override
    public void testDefinition() {
        // class
        assertHasModifiers(classDescriptor, PUBLIC);
        assertDoesNotHaveModifiers(classDescriptor, ABSTRACT);
        assertImplements(classDescriptor, monoid.className + "<java.lang.Integer>");

        // constructor
        assertHasModifiers(constructor, PUBLIC);
        assertEquals(1, classDescriptor.getDeclaredConstructors().length, className + " may only have the default constructor");

        // fields
        assertEquals(0, fields.size(), className + " must be stateless");
    }

    /**
     * Asserts that {@code IntegerAddition.zero()} returns 0.
     * @throws ReflectiveOperationException if any invocation fails
     */
    @Test
    public void testZero() throws ReflectiveOperationException {
        Object instance = newInstance();

        assertEquals(0, zero.invoke(instance), "Method did not return the correct value");
    }

    /**
     * Asserts that {@code IntegerAddition.add(Integer, Integer)} returns the sum of the two given integers.
     * @throws ReflectiveOperationException if any invocation fails
     */
    @Test
    public void testAdd() throws ReflectiveOperationException {
        Object instance = newInstance();
        int a = RANDOM.nextInt(1000), b = RANDOM.nextInt(1000);

        assertEquals(a + b, add.invoke(instance, a, b), "Method did not return the correct value");
    }
}
