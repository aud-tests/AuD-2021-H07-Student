package h07;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

/**
 * Installer for the community tests. <br><br>
 * This is a standalone class that can download all necessary files to run tests for this
 * assignment. More specifically, it can download all files whose hashes are listed in
 * {@code .test_metadata.xml}. This class can also be used to reset or rather re-download files
 * that have become corrupted or have otherwise been modified.<br><br>
 * It is important that the working directory is the same as the project root directory when
 * running {@link Installer#main(String[])}, otherwise the files might be downloaded to the wrong
 * location or even overwrite other files!
 */
public class Installer {

    // ----------------------------------- //
    // DO NOT CHANGE ANYTHING IN THIS FILE //
    // ----------------------------------- //

    /**
     * The ID of this assignment
     */
    private static final String ASSIGNMENT_ID = "H07";

    /**
     * Test installer / downloader
     * @param args An array of filenames to download. All filenames have to be relative to project root.
     *             If any are given, only those will be downloaded (and overwritten). If none are
     *             given, all files whose hashes are in {@code .test_metadata.xml} will be downloaded
     *             (and overwritten).
     */
    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            HttpResponse<String> response = getHttpResource(".test_metadata.xml");

            if (response.statusCode() != 200)
                throw new RuntimeException("Unable to fetch metadata from repository");

            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new StringReader(response.body())));
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList hashes = (NodeList) xPath.evaluate("/metadata/hashes/*", document, XPathConstants.NODESET);

            System.out.println("Installing version " + xPath.evaluate("/metadata/version", document) + " of tests for " + ASSIGNMENT_ID + "...");

            for (int i = 0; i < hashes.getLength(); i++)
                updateLocal(xPath.evaluate("@file", hashes.item(i)));
        } else {
            for (String filename : args)
                updateLocal(filename);
        }
    }

    /**
     * Requests a resource / file from the repository.
     * @param resource The resource to get from the repository.
     * @return a {@link HttpResponse<String>} object
     */
    private static HttpResponse<String> getHttpResource(String resource) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newBuilder()
                                      .version(HttpClient.Version.HTTP_2)
                                      .followRedirects(HttpClient.Redirect.NORMAL)
                                      .connectTimeout(Duration.ofSeconds(20))
                                      .build();
        HttpRequest request = HttpRequest.newBuilder(
                URI.create("https://git.rwth-aachen.de/aud-tests/AuD-2021-" + ASSIGNMENT_ID +"-Student/-/raw/master/" + resource)).build();

        return client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    /**
     * Creates (or overwrites) the specified file with the contents of the file at the repository.
     * All missing directories will be created automatically.
     * @param fileName The file with path relative to project root.
     */
    private static void updateLocal(String fileName) throws IOException, InterruptedException {
        System.out.print("Downloading " + fileName + "... ");

        File file = new File(fileName);
        HttpResponse<String> response = getHttpResource(fileName);

        //noinspection ResultOfMethodCallIgnored
        file.getParentFile().mkdirs();

        if (response != null && response.statusCode() == 200) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                writer.write(response.body());
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("done");

            return;
        }

        System.out.println("unable to fetch file from repository");
    }
}
