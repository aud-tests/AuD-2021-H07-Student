package h07.graph;

import h07.TemplateTests;
import h07.util.TestClass;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

import static h07.util.Assertions.*;
import static java.lang.reflect.Modifier.*;

/**
 * Tests for class {@code AdjacencyMatrixFactory}. <br><br>
 * Depends on {@link TemplateTests.DirectedGraphFactoryTemplateTest}.
 */
public class AdjacencyMatrixFactoryTest extends TestClass {

    /** {@code Field} object for {@code AdjacencyMatrixFactory.nodes} */
    public final Field nodes;
    /** {@code Field} object for {@code AdjacencyMatrixFactory.adjacencyMatrix} */
    public final Field adjacencyMatrix;
    /** {@code Method} object for {@link DirectedGraphFactory#createDirectedGraph()} */
    public final Method createDirectedGraph;

    /** Instance of {@link TemplateTests.DirectedGraphFactoryTemplateTest} */
    private final TemplateTests.DirectedGraphFactoryTemplateTest directedGraphFactory;

    /**
     * Creates a new {@link AdjacencyMatrixFactoryTest} object. Requires that {@code AdjacencyMatrixFactory} exists
     * and has a constructor with two parameters: the first one with type {@code V[]} and the second one
     * with type {@code A[][]}. Also requires that the class has two fields, {@code nodes} and {@code adjacencyMatrix}.
     */
    public AdjacencyMatrixFactoryTest() {
        super("h07.graph.AdjacencyMatrixFactory", constructor -> {
            Type[] parameters = constructor.getGenericParameterTypes();

            return parameters.length == 2 && parameters[0].getTypeName().equals("V[]") && parameters[1].getTypeName().equals("A[][]");
        });

        directedGraphFactory = TemplateTests.INSTANCE.new DirectedGraphFactoryTemplateTest();

        createDirectedGraph = directedGraphFactory.createDirectedGraph;

        nodes = getFieldByName("nodes");
        adjacencyMatrix = getFieldByName("adjacencyMatrix");
    }

    /**
     * Tests the definition of {@code AdjacencyMatrixFactory}.
     * <ul>
     *     <li>Asserts that the class...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is public</li>
     *         <li>is not abstract</li>
     *         <li>is generic and has the type parameters V and A</li>
     *         <li>implements {@link DirectedGraphFactory}</li>
     *     </ul></li>
     *     <li>Asserts that the constructor is public</li>
     *     <li>Asserts that the field {@code nodes}...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is private and final</li>
     *         <li>is not static</li>
     *         <li>has type {@code V[]}</li>
     *     </ul></li>
     *     <li>Asserts that the field {@code adjacencyMatrix}...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is private and final</li>
     *         <li>is not static</li>
     *         <li>has type {@code A[][]}</li>
     *     </ul></li>
     * </ul>
     */
    @Test
    @Override
    public void testDefinition() {
        // class
        assertHasModifiers(classDescriptor, PUBLIC);
        assertDoesNotHaveModifiers(classDescriptor, ABSTRACT);
        assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);
        assertImplements(classDescriptor, directedGraphFactory.className + "<V, A>");

        // constructor
        assertHasModifiers(constructor, PUBLIC);

        // fields
        assertHasModifiers(nodes, PRIVATE, FINAL);
        assertDoesNotHaveModifiers(nodes, STATIC);
        assertType(nodes, "V[]");

        assertHasModifiers(adjacencyMatrix, PRIVATE, FINAL);
        assertDoesNotHaveModifiers(adjacencyMatrix, STATIC);
        assertType(adjacencyMatrix, "A[][]");
    }

    /**
     * Tests an instance of {@code AdjacencyMatrixFactory}. <br>
     * Asserts that the fields {@code nodes} and {@code adjacencyMatrix} are initialized correctly.
     * Also asserts that a {@link NullPointerException} is thrown when the constructor is invoked with
     * {@code null} and that a {@link IllegalArgumentException} is thrown when parameters are malformed
     * (criteria "{@code length of array == number of rows == number of columns}" is not fulfilled).
     * @throws ReflectiveOperationException if any invocation fails
     */
    @Test
    @Override
    public void testInstance() throws ReflectiveOperationException {
        String[] nodes = {"München", "Augsburg", "Karlsruhe", "Erfurt"};
        Integer[][] adjacencyMatrix = {
                {null,   84, null, null},
                {  84, null,  250, null},
                {null,  250, null, null},
                {null, null, null, null}
        };

        Object instance = newInstance(nodes, adjacencyMatrix);

        // same nodes as in parameter
        assertArrayEquals(nodes, (Object[]) this.nodes.get(instance),
                "Elements of nodes differ from given array");

        Integer[][] actualAdjacencyMatrix = (Integer[][]) this.adjacencyMatrix.get(instance);

        // same weights as in parameter
        for (int i = 0; i < adjacencyMatrix.length; i++)
            assertArrayEquals(adjacencyMatrix[i], actualAdjacencyMatrix[i],
                    "Row " + i + " of adjacencyMatrix differs from given matrix");

        // parameter == null
        assertThrows(NullPointerException.class, () -> newInstanceExpectingException(null, new Integer[1][1]),
                "Constructor did not throw a NullPointerException when invoked with null as first parameter");
        assertThrows(NullPointerException.class, () -> newInstanceExpectingException(new String[] {"Hello", "world!"}, null),
                "Constructor did not throw a NullPointerException when invoked with null as second parameter");
        assertThrows(NullPointerException.class, () -> newInstanceExpectingException(new String[] {"Hello", null}, new Integer[][] {{0, 1}, {1, 0}}),
                "Constructor did not throw a NullPointerException when invoked with null-containing node array as first parameter");

        // malformed parameters
        assertThrows(IllegalArgumentException.class, () -> newInstanceExpectingException(new String[] {"Hello"}, new Integer[][] {{0, 1}, {1, 0}}),
                "Constructor did not throw an IllegalArgumentException when invoked with parameters where the node array's length " +
                        "doesn't match the number of rows / columns of the adjacency matrix");
    }

    /**
     * Tests for {@code AdjacencyMatrixFactory.createDirectedGraph()}. <br>
     * Asserts that the object returned by the tested method is actually an instance of {@code DirectedGraphImpl}.
     * @throws ReflectiveOperationException if any invocation fails
     */
    @Test
    public void testCreateDirectedGraph() throws ReflectiveOperationException {
        String[] nodes = {"Hello", "world!"};
        Integer[][] adjacencyMatrix = {
                {null,    0},
                { 100, null},
        };
        DirectedGraphImplTest directedGraphImplTest = new DirectedGraphImplTest();
        Object instance = newInstance(nodes, adjacencyMatrix);

        // result is an instance of DirectedGraphImpl
        assertEquals(directedGraphImplTest.classDescriptor, createDirectedGraph.invoke(instance).getClass(),
                "Result of createDirectedGraph() is not a DirectedGraphImpl");
    }
}
