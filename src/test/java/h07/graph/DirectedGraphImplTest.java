package h07.graph;

import h07.TemplateTests;
import h07.util.TestClass;
import h07.util.provider.GraphProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static h07.util.Assertions.*;
import static java.lang.reflect.Modifier.*;

/**
 * Tests for class {@code DirectedGraphImpl}. <br><br>
 * Depends on {@link TemplateTests.DirectedGraphTemplateTest}.
 */
@SuppressWarnings("unchecked")
public class DirectedGraphImplTest extends TestClass {

    /** {@code Method} object for {@code DirectedGraph.getAllNodes()} */
    public final Method getAllNodes;
    /** {@code Method} object for {@code DirectedGraph.getChildrenForNode(V)} */
    public final Method getChildrenForNode;
    /** {@code Method} object for {@code DirectedGraph.getArcWeightBetween(V, V)} */
    public final Method getArcWeightBetween;
    /** {@code Method} object for {@code DirectedGraph.addNode(V)} */
    public final Method addNode;
    /** {@code Method} object for {@code DirectedGraph.removeNode(V)} */
    public final Method removeNode;
    /** {@code Method} object for {@code DirectedGraph.connectNodes(V, A, V)} */
    public final Method connectNodes;
    /** {@code Method} object for {@code DirectedGraph.disconnectNodes(V, V)} */
    public final Method disconnectNodes;

    /** Instance of {@link TemplateTests.DirectedGraphTemplateTest} */
    private final TemplateTests.DirectedGraphTemplateTest directedGraph;

    /**
     * Creates a new {@link DirectedGraphImplTest} object. Requires that {@code DirectedGraphImpl} exists
     * and has a constructor with no parameters.
     */
    public DirectedGraphImplTest() {
        super("h07.graph.DirectedGraphImpl", constructor -> constructor.getParameterCount() == 0);

        directedGraph = TemplateTests.INSTANCE.new DirectedGraphTemplateTest();

        getAllNodes = directedGraph.getAllNodes;
        getChildrenForNode = directedGraph.getChildrenForNode;
        getArcWeightBetween = directedGraph.getArcWeightBetween;
        addNode = directedGraph.addNode;
        removeNode = directedGraph.removeNode;
        connectNodes = directedGraph.connectNodes;
        disconnectNodes = directedGraph.disconnectNodes;
    }

    /**
     * Tests the definition of {@code DirectedGraphImpl}.
     * <ul>
     *     <li>Asserts that the class...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is package-private</li>
     *         <li>is not abstract</li>
     *         <li>is generic and has the type parameters V and A</li>
     *         <li>implements {@link DirectedGraph}</li>
     *     </ul></li>
     *     <li>Asserts that the constructor is package-private</li>
     *     <li>Asserts that all fields are private</li>
     *     <li>Asserts that all methods that are not defined in {@link DirectedGraph} or inherited are private</li>
     *     <li>Asserts that all inner classes are private</li>
     * </ul>
     */
    @Test
    @Override
    public void testDefinition() {
        // class
        assertDoesNotHaveModifiers(classDescriptor, PUBLIC, PROTECTED, PRIVATE, ABSTRACT);
        assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);
        assertImplements(classDescriptor, directedGraph.className + "<V, A>");

        // constructor
        assertDoesNotHaveModifiers(constructor, PUBLIC, PROTECTED, PRIVATE);

        // fields
        for (Field field : fields.values())
            assertHasModifiers(field, PRIVATE);

        // methods
        methods.values()
               .stream()
               .filter(method -> !directedGraph.methods.containsKey(getMethodSignature(method)))
               .forEach(method -> assertHasModifiers(method, method.getName() + " is not overwritten and must therefore be private", PRIVATE));

        // inner classes
        for (Class<?> c : classDescriptor.getDeclaredClasses())
            assertHasModifiers(c, PRIVATE);
    }

    /**
     * Tests for {@code DirectedGraphImpl.getAllNodes()}. <br>
     * Asserts that the collection returned by the tested method is immutable and has the same size and elements as {@code nodes}.
     * @param nodes           an array of nodes (supplied by the arguments provider)
     * @param adjacencyMatrix a matrix of weights from one node to another (supplied by the arguments provider)
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(GraphProvider.class)
    public void testGetAllNodes(Character[] nodes, Integer[][] adjacencyMatrix) throws ReflectiveOperationException {
        Object instance = newInstance(nodes, adjacencyMatrix);
        Collection<Character> expectedNodes = Set.of(nodes), actualNodes = (Collection<Character>) getAllNodes.invoke(instance);

        // collection has correct size
        assertEquals(expectedNodes.size(), actualNodes.size(), "Number of nodes in the returned collection differs from expected amount");
        // collection has all nodes
        assertTrue(actualNodes.containsAll(expectedNodes), "The returned collection does not contain all nodes");

        // unmodifiable Collection
        assertThrows(UnsupportedOperationException.class, () -> actualNodes.add('0'), "Returned collection is not immutable");
        assertThrows(UnsupportedOperationException.class, () -> actualNodes.addAll(Set.of('0')), "Returned collection is not immutable");
        assertThrows(UnsupportedOperationException.class, actualNodes::clear, "Returned collection is not immutable");
        assertThrows(UnsupportedOperationException.class, () -> actualNodes.remove('0'), "Returned collection is not immutable");
        assertThrows(UnsupportedOperationException.class, () -> actualNodes.removeAll(Set.of('0')), "Returned collection is not immutable");
        assertThrows(UnsupportedOperationException.class, () -> actualNodes.retainAll(Set.of('0')), "Returned collection is not immutable");
    }

    /**
     * Tests for {@code DirectedGraphImpl.getChildrenForNode(V)}. <br>
     * Asserts that the collection returned by the tested method is immutable. Further asserts that the collection
     * has the same size and elements as the expected set of children for a given node. Also asserts that the method
     * throws a {@link NullPointerException} if invoked with {@code null} and a {@link NoSuchElementException} if
     * invoked with a node that is not in this graph.
     * @param nodes           an array of nodes (supplied by the arguments provider)
     * @param adjacencyMatrix a matrix of weights from one node to another (supplied by the arguments provider)
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(GraphProvider.class)
    public void testGetChildrenForNode(Character[] nodes, Integer[][] adjacencyMatrix) throws ReflectiveOperationException {
        Object instance = newInstance(nodes, adjacencyMatrix);

        for (int i = 0; i < nodes.length; i++) {
            Function<Integer, Integer> indexMapper = new Function<>() {
                int i = 0;

                @Override
                public Integer apply(Integer integer) {
                    return integer == null ? -(i++ + 1) : i++;
                }
            };
            Set<Character> expectedChildren = Arrays.stream(adjacencyMatrix[i])
                                                    .map(indexMapper)
                                                    .filter(integer -> integer >= 0)
                                                    .map(integer -> nodes[integer])
                                                    .collect(Collectors.toUnmodifiableSet());
            Collection<Character> actualChildren = (Collection<Character>) getChildrenForNode.invoke(instance, nodes[i]);

            // collection has correct size
            assertEquals(expectedChildren.size(), actualChildren.size(),
                    "Number of child nodes in the returned collection differs from expected amount");
            // collection has all child nodes
            assertTrue(actualChildren.containsAll(expectedChildren),
                    "The returned collection does not contain all child nodes for " + nodes[i]);

            // unmodifiable Collection
            assertThrows(UnsupportedOperationException.class, () -> actualChildren.add('0'), "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, () -> actualChildren.addAll(Set.of('0')), "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, actualChildren::clear, "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, () -> actualChildren.remove('0'), "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, () -> actualChildren.removeAll(Set.of('0')), "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, () -> actualChildren.retainAll(Set.of('0')), "Returned collection is not immutable");
        }

        // node == null
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, getChildrenForNode, (Object) null),
                "Method did not throw a NullPointerException when invoked with null");

        // node not in graph
        assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getChildrenForNode, '0'),
                "Method did not throw a NoSuchElementException when invoked with node that is not in graph");
    }

    /**
     * Tests for {@code DirectedGraphImpl.getArcWeightBetween(V, V)}. <br>
     * Asserts that the value returned by the tested method equals the expected one, should an arc exist. If there is
     * no arc from {@code nodes[i]} to {@code nodes[j]}, asserts that the method throws a {@link NoSuchElementException}.
     * Also asserts that the method throws a {@link NullPointerException} if invoked with {@code null} and a
     * {@link NoSuchElementException} if invoked with a node that is not in this graph.
     * @param nodes           an array of nodes (supplied by the arguments provider)
     * @param adjacencyMatrix a matrix of weights from one node to another (supplied by the arguments provider)
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(GraphProvider.class)
    public void testGetArcWeightBetween(Character[] nodes, Integer[][] adjacencyMatrix) throws ReflectiveOperationException {
        Object instance = newInstance(nodes, adjacencyMatrix);

        for (int i = 0; i < nodes.length; i++)
            for (int j = 0; j < nodes.length; j++)
                if (adjacencyMatrix[i][j] != null)
                    // connected nodes
                    assertEquals(adjacencyMatrix[i][j], getArcWeightBetween.invoke(instance, nodes[i], nodes[j]),
                            "Method did return the correct value for the arc weight between the nodes " + nodes[i] + " and " + nodes[j]);
                else {
                    int fixI = i, fixJ = j;

                    // unconnected nodes
                    assertThrows(
                            NoSuchElementException.class,
                            () -> invokeExpectingException(instance, getArcWeightBetween, nodes[fixI], nodes[fixJ]),
                            "Method did not throw a NoSuchElementException despite the nodes " +
                                    nodes[i] + " and " + nodes[j] + " not being connected"
                    );
                }

        // node == null
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, getArcWeightBetween, null, nodes[0]),
                "Method did not throw a NullPointerException when invoked with null as first parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, getArcWeightBetween, nodes[0], null),
                "Method did not throw a NullPointerException when invoked with null as second parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, getArcWeightBetween, null, null),
                "Method did not throw a NullPointerException when invoked with null as first and second parameter");

        // node not in graph
        assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getArcWeightBetween, '0', nodes[0]),
                "Method did not throw a NoSuchElementException when invoked with non-existing node as first parameter");
        assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getArcWeightBetween, nodes[0], '0'),
                "Method did not throw a NoSuchElementException when invoked with non-existing node as second parameter");
        assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getArcWeightBetween, '0', '1'),
                "Method did not throw a NoSuchElementException when invoked with non-existing node as first and second parameter");
    }

    /**
     * Tests for {@code DirectedGraphImpl.addNode(V)}. Requires {@code DirectedGraphImpl.getAllNodes()}. <br>
     * Asserts that the invocation of the tested method does not throw any exceptions and that a
     * {@link IllegalArgumentException} is thrown when attempting to add the same node again. After that, asserts that all
     * nodes with which {@code DirectedGraphImpl.addNode(V)} was invoked actually have been added.
     * Also asserts that the method throws a {@link NullPointerException} if invoked with {@code null}.
     * @param nodes           an array of nodes (supplied by the arguments provider)
     * @param adjacencyMatrix a matrix of weights from one node to another (supplied by the arguments provider)
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(GraphProvider.class)
    public void testAddNode(Character[] nodes, Integer[][] adjacencyMatrix) throws ReflectiveOperationException {
        Object instance = newInstance(nodes, adjacencyMatrix);
        Character[] addedNodes = RANDOM.ints(5, 0x61, 0x7B)
                                       .distinct()
                                       .mapToObj(i -> (char) i)
                                       .toArray(Character[]::new);

        for (Character node : addedNodes) {
            // node not in graph
            assertDoesNotThrow(() -> addNode.invoke(instance, node));
            // node in graph
            assertThrows(IllegalArgumentException.class, () -> invokeExpectingException(instance, addNode, node),
                    "Method did not throw an IllegalArgumentException when invoked with a duplicate node");
        }

        Collection<Character> actualNodes = (Collection<Character>) getAllNodes.invoke(instance);

        // collection has correct size
        assertEquals(nodes.length + addedNodes.length, actualNodes.size(),
                "Number of nodes in the returned collection differs from expected amount");
        // collection has all nodes + added
        assertTrue(actualNodes.containsAll(Stream.concat(Arrays.stream(nodes), Arrays.stream(addedNodes)).collect(Collectors.toUnmodifiableSet())),
                "The returned collection does not contain all added nodes");

        // node == null
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, addNode, (Object) null),
                "Method did not throw a NullPointerException when invoked with null");
    }

    /**
     * Tests for {@code DirectedGraphImpl.removeNode(V)}. Requires {@code DirectedGraphImpl.getAllNodes()} and
     * {@code DirectedGraphImpl.getChildrenForNode(V)} (only exceptions). <br>
     * Asserts that the invocation of the tested method does not throw any exceptions and that a
     * {@link NoSuchElementException} is thrown when attempting to remove the same node again. Asserts that the
     * removed node was actually removed from the collection returned by {@code DirectedGraphImpl.getAllNodes()}
     * and that all arcs to and from the node have been removed. Also asserts that the method throws a
     * {@link NullPointerException} if invoked with {@code null}.
     * @param nodes           an array of nodes (supplied by the arguments provider)
     * @param adjacencyMatrix a matrix of weights from one node to another (supplied by the arguments provider)
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(GraphProvider.class)
    public void testRemoveNode(Character[] nodes, Integer[][] adjacencyMatrix) throws ReflectiveOperationException {
        Object instance = newInstance(nodes, adjacencyMatrix);

        for (int i = 0; i < nodes.length; i++) {
            Function<Integer, Integer> indexMapper = new Function<>() {
                int i = 0;

                @Override
                public Integer apply(Integer integer) {
                    return integer == null ? -(i++ + 1) : i++;
                }
            };
            Set<Character> children = Arrays.stream(adjacencyMatrix[i])
                                            .map(indexMapper)
                                            .filter(integer -> integer >= 0)
                                            .map(integer -> nodes[integer])
                                            .collect(Collectors.toUnmodifiableSet());
            Character node = nodes[i];

            // node in graph
            assertDoesNotThrow(() -> removeNode.invoke(instance, node));
            // node not in graph
            assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, removeNode, node),
                    "Method did not throw a NoSuchElementException when invoked with node that is not in graph");
            // node not in collection of all nodes
            assertFalse(((Collection<Character>) getAllNodes.invoke(instance)).contains(node),
                    "Removed node is still in the collection returned by getAllNodes()");
            // removed node has no children
            assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getChildrenForNode, node),
                    "getChildrenForNode(V) doesn't throw a NoSuchElementException when invoked with removed node");

            // former children don't have any association with removed node
            for (Character child : children)
                assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getArcWeightBetween, node, child),
                        "getArcWeightBetween(V, V) doesn't throw a NoSuchElementException when invoked with removed node and its former children");

            for (int j = 0; j < nodes.length; j++)
                if (adjacencyMatrix[j][i] != null) {
                    int fixJ = j;

                    assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getArcWeightBetween, nodes[fixJ], node),
                            "getArcWeightBetween(V, V) doesn't throw a NoSuchElementException when invoked with " +
                                    "the former parents of a removed node and the removed node");
                }
        }

        // node == null
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, removeNode, (Object) null),
                "Method did not throw NullPointerException when invoked with null");
    }

    /**
     * Tests for {@code DirectedGraphImpl.connectNodes(V, A, V)}. Requires
     * {@code DirectedGraphImpl.getChildrenForNode(V)}. This test will attempt to connect every
     * node to itself and all others. <br>
     * Asserts that the invocation of the tested method does not throw any exceptions and that a
     * {@link IllegalArgumentException} is thrown when attempting to connect the same nodes again if
     * there was no arc between them. If there was, asserts that the method throws a {@link IllegalArgumentException}.
     * Asserts that every node in {@code nodes} has a connection to every other node and itself.
     * Also asserts that the method throws a {@link NullPointerException} if invoked with {@code null} and a
     * {@link NoSuchElementException} if invoked with a node that is not in this graph.
     * @param nodes           an array of nodes (supplied by the arguments provider)
     * @param adjacencyMatrix a matrix of weights from one node to another (supplied by the arguments provider)
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(GraphProvider.class)
    public void testConnectNodes(Character[] nodes, Integer[][] adjacencyMatrix) throws ReflectiveOperationException {
        Object instance = newInstance(nodes, adjacencyMatrix);

        for (int i = 0; i < nodes.length; i++)
            for (int j = 0; j < nodes.length; j++) {
                int fixI = i, fixJ = j;

                if (adjacencyMatrix[i][j] == null)
                    // nodes not connected
                    assertDoesNotThrow(() -> connectNodes.invoke(instance, nodes[fixI], 0, nodes[fixJ]));

                // nodes already connected
                assertThrows(IllegalArgumentException.class, () -> invokeExpectingException(instance, connectNodes, nodes[fixI], 0, nodes[fixJ]),
                        "Method did not throw an IllegalArgumentException when invoked with nodes who are already connected. " +
                                "Attempted to create an arc from " + nodes[i] + " to " + nodes[j]);
            }

        for (Character node : nodes) {
            Collection<Character> children = (Collection<Character>) getChildrenForNode.invoke(instance, node);

            // collection has correct size
            assertEquals(nodes.length, children.size(),
                    "Number of child nodes in the returned collection differs from expected amount");
            // collection has all child nodes
            assertTrue(children.containsAll(Set.of(nodes)),
                    "The returned collection does not contain all child nodes for " + node);
        }

        // node == null or weight == null
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, connectNodes, null, 0, '1'),
                "Method did not throw a NullPointerException when invoked with null as first parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, connectNodes, '0', null, '1'),
                "Method did not throw a NullPointerException when invoked with null as second parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, connectNodes, '0', 0, null),
                "Method did not throw a NullPointerException when invoked with null as third parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, connectNodes, null, null, '1'),
                "Method did not throw a NullPointerException when invoked with null as first and second parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, connectNodes, null, 0, null),
                "Method did not throw a NullPointerException when invoked with null as first and third parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, connectNodes, '0', null, null),
                "Method did not throw a NullPointerException when invoked with null as second and third parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, connectNodes, null, null, null),
                "Method did not throw a NullPointerException when invoked with null as first, second and third parameter");

        // node is not in graph
        assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, connectNodes, '0', 0, nodes[0]),
                "Method did not throw a NoSuchElementException when invoked with non-existent node as first parameter");
        assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, connectNodes, nodes[0], 0, '1'),
                "Method did not throw a NoSuchElementException when invoked with non-existent node as third parameter");
    }

    /**
     * Tests for {@code DirectedGraphImpl.disconnectNodes(V, V)}. Requires
     * {@code DirectedGraphImpl.getChildrenForNode(V)}. This test will attempt to disconnect
     * every node from itself and every other node. <br>
     * Asserts that the invocation of the tested method does not throw any exceptions and that a
     * {@link NoSuchElementException} is thrown when attempting to disconnect the same nodes again if
     * there was a arc between them. If there wasn't, asserts that the method throws a {@link NoSuchElementException}.
     * Asserts that every node in {@code nodes} has no connections to any other node nor itself.
     * Also asserts that the method throws a {@link NullPointerException} if invoked with {@code null} and a
     * {@link NoSuchElementException} if invoked with a node that is not in this graph.
     * @param nodes           an array of nodes (supplied by the arguments provider)
     * @param adjacencyMatrix a matrix of weights from one node to another (supplied by the arguments provider)
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(GraphProvider.class)
    public void testDisconnectNodes(Character[] nodes, Integer[][] adjacencyMatrix) throws ReflectiveOperationException {
        Object instance = newInstance(nodes, adjacencyMatrix);

        for (int i = 0; i < nodes.length; i++)
            for (int j = 0; j < nodes.length; j++) {
                int fixI = i, fixJ = j;

                if (adjacencyMatrix[i][j] != null)
                    // nodes connected
                    assertDoesNotThrow(() -> disconnectNodes.invoke(instance, nodes[fixI], nodes[fixJ]));

                // nodes already disconnected
                assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, disconnectNodes, nodes[fixI], nodes[fixJ]),
                        "Method did not throw an NoSuchElementException when invoked with nodes who are already disconnected. " +
                                "Attempted to remove an arc from " + nodes[i] + " to " + nodes[j]);
            }

        for (Character node : nodes) {
            Collection<Character> children = (Collection<Character>) getChildrenForNode.invoke(instance, node);

            // nodes doesn't have any children
            assertEquals(0, children.size(), "Number of child nodes in the returned collection differs from expected amount");
        }

        // node == null
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, disconnectNodes, null, '1'),
                "Method did not throw a NullPointerException when invoked with null as first parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, disconnectNodes, '0', null),
                "Method did not throw a NullPointerException when invoked with null as second parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, disconnectNodes, null, null),
                "Method did not throw a NullPointerException when invoked with null as first and second parameter");

        // node is not in graph
        assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, disconnectNodes, '0', nodes[0]),
                "Method did not throw a NoSuchElementException when invoked with non-existent node as first parameter");
        assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, disconnectNodes, nodes[0], '1'),
                "Method did not throw a NoSuchElementException when invoked with non-existent node as second parameter");
    }

    /**
     * Returns a new {@code DirectedGraphImpl} object. <br>
     * It is necessary to override {@link TestClass#newInstance(Object...)} because no
     * constructors have been specified by the assignment, so using
     * {@code AdjacencyMatrixFactory.createDirectedGraph()} is the way to go.
     * @param params the parameters to supply to the constructor of {@code AdjacencyMatrixFactory}
     * @return a new {@code DirectedGraphImpl} instance
     */
    @Override
    public Object newInstance(Object... params) {
        AdjacencyMatrixFactoryTest adjacencyMatrixFactoryTest = new AdjacencyMatrixFactoryTest();

        try {
            return adjacencyMatrixFactoryTest.createDirectedGraph.invoke(adjacencyMatrixFactoryTest.newInstance(params));
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("An exception occurred when invoking the constructor of " + adjacencyMatrixFactoryTest.className, e);
        }
    }
}
