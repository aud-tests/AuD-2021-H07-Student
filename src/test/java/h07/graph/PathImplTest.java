package h07.graph;

import h07.TemplateTests;
import h07.util.TestClass;
import h07.util.provider.PathProvider;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static h07.util.Assertions.*;
import static java.lang.reflect.Modifier.*;

/**
 * Tests for class {@code PathImpl}. <br><br>
 * Depends on {@link TemplateTests.PathTemplateTest} and {@link TemplateTests.TraverserTemplateTest}.
 */
@SuppressWarnings("unchecked")
public class PathImplTest extends TestClass {

    /** {@code Method} object for {@code Path.traverser()} */
    public final Method traverser;
    /** {@code Method} object for {@code Path.concat(V, A)} */
    public final Method concat;
    /** {@code Method} object for {@code Path.iterator()} */
    public final Method iterator;
    /** {@code Method} object for {@code Path.of(V)} */
    public final Method of1;
    /** {@code Method} object for {@code Path.of(V, A, V)} */
    public final Method of2;
    /** {@code Method} object for {@code Path.of(V, A, V, A, V)} */
    public final Method of3;

    /** Instance of {@link TemplateTests.PathTemplateTest} */
    private final TemplateTests.PathTemplateTest path;
    /** Instance of {@link TemplateTests.TraverserTemplateTest} */
    private final TemplateTests.TraverserTemplateTest _traverser;

    public PathImplTest() {
        super("h07.graph.PathImpl", null);

        path = TemplateTests.INSTANCE.new PathTemplateTest();
        _traverser = TemplateTests.INSTANCE.new TraverserTemplateTest();

        traverser = path.traverser;
        concat = path.concat;
        of1 = path.of1;
        of2 = path.of2;
        of3 = path.of3;
        iterator = path.iterator;
    }

    /**
     * Tests the definition of {@code PathImpl}.
     * <ul>
     *     <li>Asserts that the class...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is package-private</li>
     *         <li>is not abstract</li>
     *         <li>is generic and has the type parameters V and A</li>
     *         <li>either extends {@link AbstractPath} or implements {@link Path} directly</li>
     *     </ul></li>
     * </ul>
     */
    @Test
    @Override
    public void testDefinition() {
        // class
        assertDoesNotHaveModifiers(classDescriptor, PUBLIC, PROTECTED, PRIVATE, ABSTRACT);
        assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);

        if (classDescriptor.getGenericInterfaces().length == 1)
            assertImplements(classDescriptor, path.className + "<V, A>");
        else
            assertEquals("h07.graph.AbstractPath<V, A>", classDescriptor.getGenericSuperclass().getTypeName(),
                    className + " must either implement h07.graph.Path<V, A> directly or extend h07.graph.AbstractPath<V, A>");
    }

    /**
     * Tests for the {@code Traverser} returned by {@code PathImpl.traverser()}. <br><br>
     * Depends on {@link PathImplTest}.
     */
    @Nested
    public class TraverserTest {

        /** {@code Method} object for {@link Path.Traverser#getCurrentNode()} */
        public final Method getCurrentNode;
        /** {@code Method} object for {@link Path.Traverser#getDistanceToNextNode()} */
        public final Method getDistanceToNextNode;
        /** {@code Method} object for {@link Path.Traverser#walkToNextNode()} */
        public final Method walkToNextNode;
        /** {@code Method} object for {@link Path.Traverser#hasNextNode()} */
        public final Method hasNextNode;

        /**
         * Creates a new {@link TraverserTest} object.
         */
        public TraverserTest() {
            getCurrentNode = _traverser.getCurrentNode;
            getDistanceToNextNode = _traverser.getDistanceToNextNode;
            walkToNextNode = _traverser.walkToNextNode;
            hasNextNode = _traverser.hasNextNode;
        }

        /**
         * Tests for {@code Path.Traverser.getCurrentNode()} on the {@code Traverser} returned by
         * {@code PathImpl.traverser()}. <br>
         * Asserts that the tested method returns the same node the {@link Path} was instantiated
         * with when invoked multiple times.
         * @param nodes      an array of nodes (supplied by the arguments provider)
         * @param arcWeights an array of weights from a node at index {@code i} to the one at index
         *                   {@code i + 1} (unused, supplied by the arguments provider)
         * @throws ReflectiveOperationException if any invocation fails
         */
        @ParameterizedTest
        @ArgumentsSource(PathProvider.class)
        public void testGetCurrentNode(Character[] nodes, @SuppressWarnings("unused") int[] arcWeights) throws ReflectiveOperationException {
            Object instance = traverser.invoke(newInstance(nodes[0]));

            assertSame(nodes[0], getCurrentNode.invoke(instance), "Method did not return correct value");
            assertSame(nodes[0], getCurrentNode.invoke(instance), "Repeated invocation did not produce same result");
        }

        /**
         * Tests for {@code Path.Traverser.getDistanceToNextNode()} on the {@code Traverser} returned by
         * {@code PathImpl.traverser()}. <br>
         * Asserts that the tested method returns the expected value when invoked and the current
         * node is not the last one in this path. If it is, asserts that an {@link IllegalArgumentException}
         * is thrown.
         * @param nodes      an array of nodes (supplied by the arguments provider)
         * @param arcWeights an array of weights from a node at index {@code i} to the one at index
         *                   {@code i + 1} (supplied by the arguments provider)
         * @throws ReflectiveOperationException if any invocation fails
         */
        @ParameterizedTest
        @ArgumentsSource(PathProvider.class)
        public void testGetDistanceToNextNode(Character[] nodes, int[] arcWeights) throws ReflectiveOperationException {
            if (nodes.length == 1) {
                Object instance = traverser.invoke(newInstance(nodes[0]));

                assertThrows(IllegalStateException.class, () -> invokeExpectingException(instance, getDistanceToNextNode),
                        "Method did not throw an IllegalStateException when invoked while at last element");
            } else {
                Object instance = traverser.invoke(newInstance(nodes[0], arcWeights[0], nodes[1]));

                assertEquals(arcWeights[0], getDistanceToNextNode.invoke(instance),
                        "Method did not return the correct value for distance to next node");
            }
        }

        /**
         * Tests for {@code Path.Traverser.walkToNextNode()} on the {@code Traverser} returned by
         * {@code PathImpl.traverser()}. Requires
         * {@link TraverserTest#testGetCurrentNode(Character[], int[])}. <br>
         * Asserts that the tested method advances the traverser to the next node when invoked and the current
         * node is not the last one in this path. If it is, asserts that an {@link NoSuchElementException}
         * is thrown.
         * @param nodes      an array of nodes (supplied by the arguments provider)
         * @param arcWeights an array of weights from a node at index {@code i} to the one at index
         *                   {@code i + 1} (supplied by the arguments provider)
         * @throws ReflectiveOperationException if any invocation fails
         */
        @ParameterizedTest
        @ArgumentsSource(PathProvider.class)
        public void testWalkToNextNode(Character[] nodes, int[] arcWeights) throws ReflectiveOperationException {
            if (nodes.length == 1) {
                Object instance = traverser.invoke(newInstance(nodes[0]));

                assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, walkToNextNode),
                        "Method did not throw a NoSuchElementException when invoked while at last element");
            } else {
                Object instance = traverser.invoke(newInstance(nodes[0], arcWeights[0], nodes[1]));

                assertDoesNotThrow(() -> walkToNextNode.invoke(instance));
                assertSame(nodes[1], getCurrentNode.invoke(instance), "Traverser did not advance to next node");
            }
        }

        /**
         * Tests for {@code Path.Traverser.hasNextNode()} on the {@code Traverser} returned by
         * {@code PathImpl.traverser()}. <br>
         * Asserts that the tested method returns {@code true} if the current node is not the last
         * one in this path. If it is, asserts that {@code false} is returned.
         * @param nodes      an array of nodes (supplied by the arguments provider)
         * @param arcWeights an array of weights from a node at index {@code i} to the one at index
         *                   {@code i + 1} (supplied by the arguments provider)
         * @throws ReflectiveOperationException if any invocation fails
         */
        @ParameterizedTest
        @ArgumentsSource(PathProvider.class)
        public void testHasNextNode(Character[] nodes, int[] arcWeights) throws ReflectiveOperationException {
            if (nodes.length == 1) {
                Object instance = traverser.invoke(newInstance(nodes[0]));

                assertFalse((Boolean) hasNextNode.invoke(instance), "Method did not return correct value while at last element");
            } else {
                Object instance = traverser.invoke(newInstance(nodes[0], arcWeights[0], nodes[1]));

                assertTrue((Boolean) hasNextNode.invoke(instance), "Method did not return correct value while not at last element");
            }
        }
    }

    /**
     * Tests for {@code PathImpl.concat(Object, Object)}. Requires {@code PathImpl.traverser()}. <br>
     * Asserts that the object returned by the tested method is a new {@code PathImpl} object.
     * Further asserts that the order and weights of nodes didn't change and the new node was appended to the end.
     * Also asserts that the method throws a {@link NullPointerException} if invoked with {@code null}.
     * @param nodes      an array of nodes (supplied by the arguments provider)
     * @param arcWeights an array of weights from a node at index {@code i} to the one at index
     *                   {@code i + 1} (supplied by the arguments provider)
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(PathProvider.class)
    public void testConcat(Character[] nodes, int[] arcWeights) throws ReflectiveOperationException {
        Object concatPath = newInstance(nodes[0]), previousPath;

        for (int i = 1, j = 0; i < nodes.length; i++, j = 0) {
            previousPath = concatPath;
            concatPath = concat.invoke(concatPath, nodes[i], arcWeights[i - 1]);

            // new path != old path
            assertNotSame(previousPath, concatPath, "Method must return a new Path object, not modify this instance");

            for (Object traverser = this.traverser.invoke(concatPath); (Boolean) _traverser.hasNextNode.invoke(traverser);
                 _traverser.walkToNextNode.invoke(traverser), j++) {
                // traverse path - nodes correct?
                assertSame(nodes[j], _traverser.getCurrentNode.invoke(traverser),
                        "Traverser did not return the correct node. Maybe the concatenation was faulty?");

                if ((Boolean) _traverser.hasNextNode.invoke(traverser))
                    // traverse path - weights correct?
                    assertEquals(arcWeights[j], _traverser.getDistanceToNextNode.invoke(traverser),
                            "Traverser did not return the correct distance to the next node. Maybe the concatenation was faulty?");
            }
        }

        Object instance = concatPath;

        // node == null or weight == null
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, concat, null, '1'),
                "Method did not throw a NullPointerException when invoked with null as first parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, concat, '0', null),
                "Method did not throw a NullPointerException when invoked with null as second parameter");
        assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, concat, null, null),
                "Method did not throw a NullPointerException when invoked with null as first and second parameter");
    }

    /**
     * Tests for {@code PathImpl.iterator()}. <br>
     * Asserts that the iterator returned by the tested method iterates through all given nodes
     * and throws a {@link NoSuchElementException} when the iterator has reached its end.
     * @throws ReflectiveOperationException if any invocation fails
     */
    @Test
    public void testIterator() throws ReflectiveOperationException {
        int i = 0;
        Character[] nodes = {'0', '1', '2'};
        Iterator<Character> iterator = (Iterator<Character>) this.iterator.invoke(newInstance(nodes[0], 0, nodes[1], 0, nodes[2]));

        while (iterator.hasNext())
            // iterate through path
            assertSame(nodes[i++], iterator.next());

        // end of iterator
        assertThrows(NoSuchElementException.class, iterator::next);
    }

    /**
     * Returns a new {@code PathImpl} object. <br>
     * It is necessary to override {@link TestClass#newInstance(Object...)} because no
     * constructors have been specified by the assignment, so using {@code Path.of(V)},
     * {@code Path.of(V, A, V)} or {@code Path.of(V, A, V, A, V)}
     * is the way to go.
     * @param params the parameters to supply to the constructor, either 1, 3 or 5 parameters
     *               as specified by the static initializer methods of {@link Path}
     * @return a new {@code PathImpl} instance
     */
    @Override
    public Object newInstance(Object... params) {
        try {
            switch (params.length) {
                case 1:
                    return of1.invoke(null, params);

                case 3:
                    return of2.invoke(null, params);

                case 5:
                    return of3.invoke(null, params);

                default:
                    throw new IllegalArgumentException("Parameters must either have length 1, 3 or 5 (see h07.graph.Path)");
            }
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("An exception occurred when trying to create a new PathImpl object (also see Path.of(...))", e);
        }
    }
}
