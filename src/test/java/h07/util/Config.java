package h07.util;

import org.junit.jupiter.params.provider.Arguments;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Contains constants that are used as settings for tests and utility classes.
 * They will not be overwritten when an update is downloaded (assuming this file is in {@link Config#EXCLUDED_FILES}),
 * but will be updated is such a way that everything between the lines containing "&gt;&gt;&gt;##" and "##&lt;&lt;&lt;" will be kept.
 * This includes any changes or appended code.
 */
public class Config {

    // >>>## UPDATE MARKER, DO NOT REMOVE, ONLY MODIFY THE LINES BELOW

    /**
     * Seed that is used for initialization of {@link TestClass#RANDOM}. <br>
     * Set to a fixed value other than {@code null} for (hopefully) reproducible results.
     */
    public static final Long SEED = null;

    /**
     * Setting for {@link Updater}. <br>
     * If set to {@code true}, the updater will check for updates whenever the tests are run.
     */
    public static final boolean CHECK_FOR_UPDATES = true;

    /**
     * Setting for {@link Updater}. <br>
     * If set to {@code true}, the updater will automatically download the most recent files from the repository
     * if an update is available. Enabling this option will overwrite files with matching names / paths, including
     * those added by the installer or previously existing files. This means that local changes will also be lost.
     * Specific files can be excluded by adding them to {@link Config#EXCLUDED_FILES}.
     */
    public static final boolean AUTO_UPDATE = true;

    /**
     * A list of files (with path relative to project root) to be excluded from updates.
     * This does not prevent updates to this configuration file ({@link Config#AUTO_UPDATE} does that),
     * it just prevents complete overwrites.
     */
    public static final List<String> EXCLUDED_FILES = List.of(
            "src/test/java/h07/util/Config.java"
    );

    /**
     * The number of times to repeat random parameterized tests.
     * This value is overridden by {@link Config#NUMBER_OF_TEST_RUNS}.
     * The default value is 5.
     */
    public static final int DEFAULT_NUMBER_OF_TEST_RUNS = 5;

    /**
     * Allows customization of the number of test runs for a parameterized test.
     * To override the number of runs, add an entry consisting of the type name + '#' + the method signature
     * (again with type names) mapped to an integer value (example below).
     * If the method is not in this map, the default value is used.
     */
    public static final Map<String, Integer> NUMBER_OF_TEST_RUNS = Map.of(
            // "h07.AbcTest#testMethod(java.lang.String)", 10

            // Arguments for this test are not randomized
            "h07.algorithm.DijkstraTest#testShortestPaths(java.lang.String, java.util.Map<java.lang.String, java.lang.String>)", 10
    );

    /**
     * Allows injection of custom arguments into a parameterized test.
     * Like for {@link Config#NUMBER_OF_TEST_RUNS}, the key is the type name of the test class, followed by '#' and the
     * method signature. The value is a stream of arguments. These arguments must match the number and types of the test
     * method's parameters. Injected arguments will always be tested first. If the number of test runs n (default or
     * adjusted) is less than the number of arguments in the stream, tests will be run with the first n arguments.
     * The number and types of arguments must match the ones expected by the test method.
     */
    public static final Map<String, Stream<? extends Arguments>> INJECTED_ARGUMENTS = Map.of(
            // "h07.AbcTest#testMethod(java.lang.String)", Stream.of(Arguments.of(..., ...), ...)
    );

    /**
     * Allows to set the name of a field manually.
     * For fields that don't have a fixed name, a selection by criteria can be used to find that field. In some cases
     * this might not yield usable results e.g., because there may be multiple fields that match a predicate. In this
     * case the name of the field in the implementation may be specified manually. The key is the type name of the
     * test class, followed by '#' and the identifier passed to {@link TestClass#getFieldByCriteria(String, Predicate)}
     * (usually the name of the field in the test class). The value is the actual name of the field (case-sensitive).
     */
    public static final Map<String, String> FIELD_NAME = Map.of(
            // "h07.AbcTest#identifier", "testField"
    );

    // ##<<< UPDATE MARKER, DO NOT REMOVE, DO NOT CHANGE ANYTHING BELOW THIS LINE

    /**
     * Returns a set of the names of all fields in this class.
     * @return the set
     */
    public static Set<String> getConfigs() {
        return Arrays.stream(Config.class.getDeclaredFields()).map(Field::getName).collect(Collectors.toUnmodifiableSet());
    }
}
