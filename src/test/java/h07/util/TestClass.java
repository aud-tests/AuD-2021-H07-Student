package h07.util;

import org.opentest4j.AssertionFailedError;
import org.opentest4j.TestAbortedException;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static h07.util.Config.CHECK_FOR_UPDATES;
import static h07.util.Config.FIELD_NAME;
import static h07.util.Updater.ASSIGNMENT_ID;

/**
 * Common class for all test classes. <br><br>
 * The centerpiece of this testsuite, which must be extended by all test classes.
 */
@SuppressWarnings("unused")
abstract public class TestClass {

    // ----------------------------------- //
    // DO NOT CHANGE ANYTHING IN THIS FILE //
    // ----------------------------------- //

    /**
     * The seed to instantiate {@link TestClass#RANDOM} with. <br>
     * If no seed has been set in {@link Config#SEED}, a random one will be generated. In any case,
     * the seed will be printed to standard output.
     */
    @SuppressWarnings("ConstantConditions")
    private static final long SEED = Config.SEED == null ? new Random().nextLong() : Config.SEED;

    /**
     * The {@link Random} instance used by all randomized tests / providers.
     */
    public static final Random RANDOM = new Random(SEED);

    /**
     * The fully qualified type name of the class that is tested
     */
    public final String className;

    /**
     * The {@link Class} object representing the actual class that is tested
     */
    public final Class<?> classDescriptor;

    /**
     * The {@link Constructor} object representing the constructor of the tested class. <br>
     * This constant may be {@code null} if a class has no constructors (e.g., because
     * {@link TestClass#classDescriptor} actually represents an interface) or if the tested class
     * shouldn't be instantiated. If this object is not {@code null}, it will have been made accessible
     * by invoking {@link Constructor#setAccessible(boolean)} with first parameter = {@code true}.
     */
    public final Constructor<?> constructor;

    /**
     * A map with all declared fields of the tested class. <br>
     * The keys of this map are the field's identifiers; the values are their respective {@link Field}
     * objects. All fields have been set to be accessible by invoking {@link Field#setAccessible(boolean)}
     * with first parameter = {@code true} on each entry.
     */
    public final Map<String, Field> fields;

    /**
     * A map with all declared methods of the tested class. <br>
     * The keys of this map are the method's signatures; the values are their respective {@link Method}
     * objects. All methods have been set to be accessible by invoking
     * {@link Method#setAccessible(boolean)} with first parameter = {@code true} on each entry.
     * @see TestClass#getMethodSignature(Method)
     */
    public final Map<String, Method> methods;

    static {
        System.out.println("Reminder: remove these tests before submitting!");

        try {
            if (!CHECK_FOR_UPDATES || !Updater.checkForUpdates()) {
                System.out.println("Seed: " + SEED);
            } else {
                System.out.println("Updated tests, please re-run");

                System.exit(0);
            }
        } catch (Exception e) {
            e.printStackTrace();

            System.exit(1);
        }
    }

    /**
     * Initializes a test class. <br>
     * Sets the fields {@link TestClass#className}, {@link TestClass#classDescriptor},
     * {@link TestClass#constructor}, {@link TestClass#fields} and {@link TestClass#methods}.
     * @param className            The name of the class / interface to be tested.
     * @param constructorPredicate A predicate to find a specific constructor of the tested class.
     *                             May be {@code null} if none is needed.
     * @throws TestAbortedException if the specified class was not found
     * @throws MissingMemberException if no constructor matching {@code constructorPredicate} has been found
     */
    protected TestClass(String className, Predicate<Constructor<?>> constructorPredicate) {
        this.className = className;
        this.classDescriptor = getClassForName(className);
        this.constructor = constructorPredicate == null ?
                null :
                Arrays.stream(classDescriptor.getDeclaredConstructors())
                      .filter(constructorPredicate)
                      .findFirst()
                      .orElseThrow(() -> new MissingMemberException(className + " is missing a constructor matching all given criteria"));
        this.fields = Collections.unmodifiableMap(new HashMap<>() {{
            for (Field field : classDescriptor.getDeclaredFields())
                if (!field.isSynthetic()) {
                    field.setAccessible(true);

                    put(field.getName(), field);
                }
        }});
        this.methods = Collections.unmodifiableMap(new HashMap<>() {{
            for (Method method : classDescriptor.getDeclaredMethods())
                if (!method.isSynthetic()) {
                    method.setAccessible(true);

                    put(getMethodSignature(method), method);
                }
        }});

        if (this.constructor != null)
            this.constructor.setAccessible(true);
    }

    /**
     * Tests the definition of {@link TestClass#classDescriptor}.
     * Implementations must be annotated with {@link org.junit.jupiter.api.Test}.
     * @throws AssertionFailedError if any assertion failed
     */
    abstract public void testDefinition();

    /**
     * Tests an instance of {@link TestClass#classDescriptor}.
     * Implementations must be annotated with {@link org.junit.jupiter.api.Test}.
     * @throws AssertionFailedError if any assertion failed
     */
    public void testInstance() throws Exception {
        throw new UnsupportedOperationException("This test class does not support tests on an instance of " + className);
    }

    /**
     * Returns the class object for a given name.
     * @param className The fully qualified type name of the class.
     * @return the class object for the corresponding name
     * @throws TestAbortedException if the class was not found
     */
    public static Class<?> getClassForName(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new TestAbortedException("Class " + e.getMessage() + " not found", e);
        }
    }

    /**
     * Returns the signature of the given method.
     * Parameter types are mapped to their respective fully qualified type names.
     * The returned string for this method would be: <br>
     * "{@code getMethodSignature(java.lang.reflect.Method)}".
     * @param method The method to generate the signature of.
     * @return the method signature
     */
    public static String getMethodSignature(Method method) {
        return method.getName() + '(' + Arrays.stream(method.getGenericParameterTypes())
                                              .map(Type::getTypeName)
                                              .collect(Collectors.joining(", ")) +
                                  ')';
    }

    /**
     * Gets the field for a given name from {@link TestClass#fields}.
     * @param fieldName The name of the field.
     * @return the field
     * @throws MissingMemberException if the field does not exist under that name
     */
    public Field getFieldByName(String fieldName) {
        Field field = fields.get(fieldName);

        if (field == null)
            throw new MissingMemberException("Field " + fieldName + " was not found in " + className);

        return field;
    }

    /**
     * Gets the field matching a given predicate from {@link TestClass#fields}.
     * @param identifier A string identifying a field in the test class.
     * @param predicate  The predicate to test with.
     * @return the field
     * @throws MissingMemberException if no fields match the given predicate
     * @throws RuntimeException if the number of fields matching the predicate is greater than 1
     */
    public Field getFieldByCriteria(String identifier, Predicate<Field> predicate) {
        if (FIELD_NAME.containsKey(getClass().getTypeName() + '#' + identifier))
            return getFieldByName(FIELD_NAME.get(getClass().getTypeName() + '#' + identifier));

        List<Field> fieldCandidates = fields.values()
                                            .stream()
                                            .filter(predicate)
                                            .collect(Collectors.toUnmodifiableList());

        if (fieldCandidates.size() == 0)
            throw new MissingMemberException("No fields matching the given predicate have been found in " + className);
        else if (fieldCandidates.size() > 1)
            throw new RuntimeException(
                    "The predicate matched more than one field and this method is unable to resolve this ambiguity.\n" +
                    "Please resolve this manually by adding the semantically correct field to the FIELD_NAME constant " +
                    "in h07.util.Config. The following fields match the predicate:\n" +
                    fieldCandidates.stream().map(Field::getName).collect(Collectors.joining("\n"))
            );
        else
            return fieldCandidates.get(0);
    }

    /**
     * Gets the method for a given signature from {@link TestClass#methods}.
     * @param methodSignature The name of the method.
     * @return the method
     * @throws MissingMemberException if the method does not exist under that name
     */
    public Method getMethodByName(String methodSignature) {
        Method method = methods.get(methodSignature);

        if (method == null)
            throw new MissingMemberException("Method " + methodSignature + " was not found in " + className);

        return method;
    }

    /**
     * Gets the method matching a given predicate from {@link TestClass#methods}.
     * @param predicate The predicate to test with.
     * @return the method
     * @throws MissingMemberException if no methods match the given predicate
     */
    public Method getMethodByCriteria(String identifier, Predicate<Method> predicate) {
        for (Method method : methods.values())
            if (predicate.test(method))
                return method;

        throw new MissingMemberException("No methods matching the given predicate have been found in " + className);
    }

    /**
     * Creates a new instance of the tested class.
     * @param params The parameters to invoke the constructor with.
     * @return the instance
     * @throws NullPointerException if {@link TestClass#constructor} is {@code null}
     * @throws RuntimeException if any exception occurred when invoking the constructor
     */
    public Object newInstance(Object... params) {
        try {
            return constructor.newInstance(params);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("An exception occurred when invoking the constructor of " + className, e);
        }
    }

    /**
     * Creates a new instance of the tested class and passes along any exceptions.
     * @param params The parameters to supply to the constructor.
     * @return the instance
     * @throws NullPointerException if {@link TestClass#constructor} is {@code null}
     * @throws Throwable any exceptions thrown by the constructor
     */
    @SuppressWarnings("UnusedReturnValue")
    public Object newInstanceExpectingException(Object... params) throws Throwable {
        try {
            return constructor.newInstance(params);
        } catch (ReflectiveOperationException e) {
            throw e.getCause();
        }
    }

    /**
     * Invokes the given method with {@code params} in the context of {@code instance} and returns the result.
     * @param instance The instance the method should be invoked on.
     * @param method   The method to invoke.
     * @param params   The parameters to invoke the method with.
     * @throws Throwable any exceptions thrown by the method
     */
    public void invokeExpectingException(Object instance, Method method, Object... params) throws Throwable {
        try {
            method.invoke(instance, params);
        } catch (ReflectiveOperationException e) {
            throw e.getCause();
        }
    }

    /**
     * An exception class to indicate that a member was not found when searching by name or criteria.
     */
    public static class MissingMemberException extends RuntimeException {

        /**
         * The error message
         */
        private final String message;

        /**
         * Creates a {@link MissingMemberException} with {@code message} as error message.
         * @param message The error message.
         */
        public MissingMemberException(String message) {
            this.message = message;

            setStackTrace(Arrays.stream(getStackTrace())
                                .filter(stackTraceElement -> stackTraceElement.getClassName().startsWith(ASSIGNMENT_ID.toLowerCase()))
                                .toArray(StackTraceElement[]::new));
        }

        @Override
        public String getMessage() {
            return message;
        }
    }
}
