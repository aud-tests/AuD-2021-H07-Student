package h07.util;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.math.BigInteger;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.*;

import static h07.util.Config.*;

/**
 * Updates the local tests if configured to do so. <br><br>
 * Assuming {@link Config#CHECK_FOR_UPDATES} is true, {@code .test_metadata.xml} is fetched from
 * the repository. If the version-tag in that file is newer than the version of the local files,
 * a message is printed to let the user know that an update is available. If
 * {@link Config#AUTO_UPDATE} is true, the MD5-hash of each file will be compared with the one in
 * {@code .test_metadata.xml} when an update is available. If the hashes don't match and
 * {@link Config#AUTO_UPDATE} is true, the local file will be overwritten with the contents of the
 * respective file from the repository. These tasks will be skipped for files that are in
 * {@link Config#EXCLUDED_FILES}.
 */
public class Updater {

    // ----------------------------------- //
    // DO NOT CHANGE ANYTHING IN THIS FILE //
    // ----------------------------------- //

    /**
     * A {@link String} representing the version of currently installed tests in SemVer format
     */
    public static final String LOCAL_VERSION = "1.1.0";

    /**
     * The ID of this assignment
     */
    public static final String ASSIGNMENT_ID = "H07";

    /**
     * The URL of the repository to use
     */
    public static final String REPOSITORY_URL = "https://git.rwth-aachen.de/aud-tests/AuD-2021-" + ASSIGNMENT_ID + "-Student/-/";

    /**
     * The root node of {@code .test_metadata.xml}
     */
    private static Node root;

    /**
     * A XPath instance
     */
    private static XPath xPath;

    /**
     * Checks if tests in the repository are newer than the local copy.
     * If {@link Config#AUTO_UPDATE} is {@code true}, hashes of local files will be generated for all
     * files in {@code .test_metadata.xml} and compared to those in the file. If a mismatch occurs,
     * the file will be downloaded from the repository and the contents of the local file will be
     * overwritten with the ones from the remote file.
     * @return whether any changes have been written to disk
     * @see Config#CHECK_FOR_UPDATES
     */
    public static boolean checkForUpdates() throws Exception {
        HttpResponse<String> response = getHttpResource(".test_metadata.xml");
        boolean persistentChanges = false;

        if (response == null || response.statusCode() != 200) {
            System.err.println("Unable to fetch version from repository");
            return false;
        }

        Document document = DocumentBuilderFactory.newInstance()
                                                  .newDocumentBuilder()
                                                  .parse(new InputSource(new StringReader(response.body())));
        xPath = XPathFactory.newInstance().newXPath();
        root = (Node) xPath.evaluate("/metadata", document, XPathConstants.NODE);
        Version localVersion = new Version(LOCAL_VERSION),
                remoteVersion = new Version(xPath.evaluate("version", root));

        if (remoteVersion.compareTo(localVersion) <= 0) {
            System.out.println("Tests are up to date");

            return false;
        }

        System.out.println("Update available! Local version: " + localVersion + " -- Remote version: " + remoteVersion);
        System.out.println("Changelog: " + REPOSITORY_URL + "blob/master/changelog.md");
        System.out.println(xPath.evaluate("message", root));

        if (!AUTO_UPDATE)
            return false;

        NodeList hashes = (NodeList) xPath.evaluate("hashes/*", root, XPathConstants.NODESET);

        for (int i = 0; i < hashes.getLength(); i++) {
            String fileName = xPath.evaluate("@file", hashes.item(i)),
                   expectedHash = hashes.item(i).getTextContent();
            File file = new File(fileName);

            if (EXCLUDED_FILES.contains(fileName))
                continue;

            if (!file.exists() || !getHash(file).equals(expectedHash))
                persistentChanges = updateLocal(file);
        }

        Set<String> actualConstants = Config.getConfigs();
        NodeList requiredConstants = (NodeList) xPath.evaluate("configuration/constants/*", root, XPathConstants.NODESET);

        for (int i = 0; i < requiredConstants.getLength(); i++)
            if (!actualConstants.contains(xPath.evaluate("@name", requiredConstants.item(i)))) {
                updateConfig();

                return true;
            }

        return persistentChanges;
    }

    /**
     * Requests a resource / file from the repository.
     * @param resource The resource to fetch from the repository.
     * @return a {@link HttpResponse<String>} object for the requested resource
     * @see Updater#REPOSITORY_URL
     */
    private static HttpResponse<String> getHttpResource(String resource) {
        HttpClient client = HttpClient.newBuilder()
                                      .version(HttpClient.Version.HTTP_2)
                                      .followRedirects(HttpClient.Redirect.NORMAL)
                                      .connectTimeout(Duration.ofSeconds(20))
                                      .build();
        HttpRequest request = HttpRequest.newBuilder(URI.create(REPOSITORY_URL + "raw/master/" + resource)).build();

        try {
            return client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Calculates the MD5-hash of a file and returns it.
     * @param file The file to generate the hash of.
     * @return the hash as a hexadecimal string
     */
    private static String getHash(File file) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");

            try (InputStream inputStream = new FileInputStream(file)) {
                String actualHash = new BigInteger(1, messageDigest.digest(inputStream.readAllBytes())).toString(16);

                return "0".repeat(32 - actualHash.length()) + actualHash;
            }
        } catch (NoSuchAlgorithmException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Updates (overwrites) the specified file with the contents of the file at the repository.
     * @param file The relative path to the file.
     * @return whether any persistent changes have been written to disk
     */
    private static boolean updateLocal(File file) {
        System.out.print("Updating " + file + "... ");

        HttpResponse<String> response = getHttpResource(file.getPath());

        //noinspection ResultOfMethodCallIgnored
        file.getParentFile().mkdirs();

        if (response != null && response.statusCode() == 200) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                writer.write(response.body());
            } catch (IOException e) {
                e.printStackTrace();
                return true;
            }

            System.out.println("done");

            return true;
        }

        System.out.println("unable to fetch file from repository");

        return false;
    }

    /**
     * Updates the configuration file, keeping all data written between the markers "&gt;&gt;&gt;##"
     * and "##&lt;&lt;&lt;".
     * @see Config
     */
    private static void updateConfig() throws XPathExpressionException {
        Node configNode = (Node) xPath.evaluate("configuration", root, XPathConstants.NODE);
        String configStub = xPath.evaluate("stub", configNode).trim() + "\n";
        File configFile = new File(xPath.evaluate("@file", configNode));
        StringBuilder configFileContents = new StringBuilder("    // >>>## UPDATE MARKER, DO NOT REMOVE, ONLY MODIFY THE LINES BELOW\n");
        Set<String> existingFields = Config.getConfigs();

        try (BufferedReader reader = new BufferedReader(new FileReader(configFile))) {
            boolean insert = false;

            for (String line = reader.readLine(); !line.matches("\\s*// ##<<<.*"); line = reader.readLine()) {
                if (line.matches("\\s*// >>>##.*")) {
                    line = reader.readLine();
                    insert = true;
                }

                if (insert)
                    configFileContents.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        StringJoiner constantsJoiner = new StringJoiner("\n\n"),
                     methodsJoiner = new StringJoiner("\n\n");
        NodeList constantsNodeList = (NodeList) xPath.evaluate("constants/*", configNode, XPathConstants.NODESET),
                 methodsNodeList = (NodeList) xPath.evaluate("methods/*", configNode, XPathConstants.NODESET);

        for (int i = 0; i < constantsNodeList.getLength(); i++)
            if (!existingFields.contains(xPath.evaluate("@name", constantsNodeList.item(i))))
                constantsJoiner.add(constantsNodeList.item(i).getTextContent().trim());

        for (int i = 0; i < methodsNodeList.getLength(); i++)
            methodsJoiner.add(methodsNodeList.item(i).getTextContent().trim());

        configFileContents.append(constantsJoiner)
                          .append("    // ##<<< UPDATE MARKER, DO NOT REMOVE, DO NOT CHANGE ANYTHING BELOW THIS LINE\n\n")
                          .append(methodsJoiner);

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(configFile))) {
            writer.write(configStub.replaceFirst(">>>##<<<", configFileContents.toString()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * A limited implementation of Semantic Versioning. <br><br>
     * This class can handle major, minor and patch versions and compare them.
     */
    private static class Version implements Comparable<Version> {

        /**
         * The major version of the version represented by an instance of this class
         */
        private final int MAJOR_VERSION;

        /**
         * The minor version of the version represented by an instance of this class
         */
        private final int MINOR_VERSION;

        /**
         * The patch version of the version represented by an instance of this class
         */
        private final int PATCH_VERSION;

        /**
         * Creates a new {@link Version} representing the given version string.
         * @param version The version as a {@link String}, e.g., "1.0.0".
         */
        private Version(String version) {
            String[] versions = version.split("\\.");

            MAJOR_VERSION = Integer.parseInt(versions[0]);
            MINOR_VERSION = Integer.parseInt(versions[1]);
            PATCH_VERSION = Integer.parseInt(versions[2]);
        }

        /**
         * Compares this {@link Version} object with the specified {@link Version} object for order
         * @param version The {@link Version} object to compare.
         * @return a negative integer, zero, or a positive integer as this object is less than, equal
         * to, or greater than the specified object
         */
        @Override
        public int compareTo(Version version) {
            int versionDiffMajor = MAJOR_VERSION - version.MAJOR_VERSION,
                versionDiffMinor = MINOR_VERSION - version.MINOR_VERSION,
                versionDiffPatch = PATCH_VERSION - version.PATCH_VERSION;

            if (versionDiffMajor != 0)
                return versionDiffMajor;
            else if (versionDiffMinor != 0)
                return versionDiffMinor;
            else
                return versionDiffPatch;
        }

        /**
         * Returns this {@link Version} object's string representation
         * @return the version as a string in SemVer format
         */
        @Override
        public String toString() {
            return MAJOR_VERSION + "." + MINOR_VERSION + "." + PATCH_VERSION;
        }
    }
}
