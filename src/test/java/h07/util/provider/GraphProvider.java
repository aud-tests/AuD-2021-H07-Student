package h07.util.provider;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

import static h07.util.TestClass.RANDOM;

public class GraphProvider extends Provider {

    @Override
    protected Stream<? extends Arguments> provideDefaultArguments(ExtensionContext context) {
        return Stream.generate(() -> {
            int n = RANDOM.nextInt(10) + 1;
            Character[] nodes = RANDOM.ints(0x41, 0x5B)
                                      .distinct()
                                      .limit(n)
                                      .mapToObj(i -> (char) i)
                                      .toArray(Character[]::new);
            Integer[][] adjacencyMatrix = new Integer[n][n];

            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    adjacencyMatrix[i][j] = RANDOM.nextBoolean() ? RANDOM.nextInt(1000) : null;

            return Arguments.of(nodes, adjacencyMatrix);
        });
    }
}
