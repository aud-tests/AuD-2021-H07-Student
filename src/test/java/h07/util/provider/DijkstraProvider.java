package h07.util.provider;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;

import java.util.Map;
import java.util.stream.Stream;

public class DijkstraProvider extends Provider {

    /** The correct shortest paths from any point in the used graphs to any other */
    private static final Map<String, Map<String, String>> PATHS = Map.of(
            "München",
            Map.of(
                    "Nürnberg",  "München -167-> Nürnberg",
                    "Frankfurt", "München -167-> Nürnberg -103-> Würzburg -217-> Frankfurt",
                    "Augsburg",  "München -84-> Augsburg",
                    "Würzburg",  "München -167-> Nürnberg -103-> Würzburg",
                    "Kassel",    "München -502-> Kassel",
                    "Stuttgart", "München -167-> Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "München -84-> Augsburg -250-> Karlsruhe",
                    "Erfurt",    "München -167-> Nürnberg -103-> Würzburg -186-> Erfurt",
                    "München",   "München",
                    "Mannheim",  "München -84-> Augsburg -250-> Karlsruhe -80-> Mannheim"
            ),
            "Augsburg",
            Map.of(
                    "Nürnberg",  "Augsburg -84-> München -167-> Nürnberg",
                    "Frankfurt", "Augsburg -250-> Karlsruhe -80-> Mannheim -85-> Frankfurt",
                    "Augsburg",  "Augsburg",
                    "Würzburg",  "Augsburg -84-> München -167-> Nürnberg -103-> Würzburg",
                    "Kassel",    "Augsburg -84-> München -502-> Kassel",
                    "Stuttgart", "Augsburg -84-> München -167-> Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "Augsburg -250-> Karlsruhe",
                    "Erfurt",    "Augsburg -84-> München -167-> Nürnberg -103-> Würzburg -186-> Erfurt",
                    "München",   "Augsburg -84-> München",
                    "Mannheim",  "Augsburg -250-> Karlsruhe -80-> Mannheim"
            ),
            "Karlsruhe",
            Map.of(
                    "Nürnberg",  "Karlsruhe -80-> Mannheim -85-> Frankfurt -217-> Würzburg -103-> Nürnberg",
                    "Frankfurt", "Karlsruhe -80-> Mannheim -85-> Frankfurt",
                    "Augsburg",  "Karlsruhe -250-> Augsburg",
                    "Würzburg",  "Karlsruhe -80-> Mannheim -85-> Frankfurt -217-> Würzburg",
                    "Kassel",    "Karlsruhe -80-> Mannheim -85-> Frankfurt -173-> Kassel",
                    "Stuttgart", "Karlsruhe -80-> Mannheim -85-> Frankfurt -217-> Würzburg -103-> Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "Karlsruhe",
                    "Erfurt",    "Karlsruhe -80-> Mannheim -85-> Frankfurt -217-> Würzburg -186-> Erfurt",
                    "München",   "Karlsruhe -250-> Augsburg -84-> München",
                    "Mannheim",  "Karlsruhe -80-> Mannheim"
            ),
            "Erfurt",
            Map.of(
                    "Nürnberg",  "Erfurt -186-> Würzburg -103-> Nürnberg",
                    "Frankfurt", "Erfurt -186-> Würzburg -217-> Frankfurt",
                    "Würzburg",  "Erfurt -186-> Würzburg",
                    "Augsburg",  "Erfurt -186-> Würzburg -103-> Nürnberg -167-> München -84-> Augsburg",
                    "Kassel",    "Erfurt -186-> Würzburg -217-> Frankfurt -173-> Kassel",
                    "Stuttgart", "Erfurt -186-> Würzburg -103-> Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "Erfurt -186-> Würzburg -217-> Frankfurt -85-> Mannheim -80-> Karlsruhe",
                    "Erfurt",    "Erfurt",
                    "München",   "Erfurt -186-> Würzburg -103-> Nürnberg -167-> München",
                    "Mannheim",  "Erfurt -186-> Würzburg -217-> Frankfurt -85-> Mannheim"
            ),
            "Nürnberg",
            Map.of(
                    "Nürnberg",  "Nürnberg",
                    "Frankfurt", "Nürnberg -103-> Würzburg -217-> Frankfurt",
                    "Würzburg",  "Nürnberg -103-> Würzburg",
                    "Augsburg",  "Nürnberg -167-> München -84-> Augsburg",
                    "Kassel",    "Nürnberg -103-> Würzburg -217-> Frankfurt -173-> Kassel",
                    "Stuttgart", "Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "Nürnberg -103-> Würzburg -217-> Frankfurt -85-> Mannheim -80-> Karlsruhe",
                    "Erfurt",    "Nürnberg -103-> Würzburg -186-> Erfurt",
                    "München",   "Nürnberg -167-> München",
                    "Mannheim",  "Nürnberg -103-> Würzburg -217-> Frankfurt -85-> Mannheim"
            ),
            "Kassel",
            Map.of(
                    "Nürnberg",  "Kassel -173-> Frankfurt -217-> Würzburg -103-> Nürnberg",
                    "Frankfurt", "Kassel -173-> Frankfurt",
                    "Würzburg",  "Kassel -173-> Frankfurt -217-> Würzburg",
                    "Augsburg",  "Kassel -502-> München -84-> Augsburg",
                    "Kassel",    "Kassel",
                    "Stuttgart", "Kassel -173-> Frankfurt -217-> Würzburg -103-> Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "Kassel -173-> Frankfurt -85-> Mannheim -80-> Karlsruhe",
                    "Erfurt",    "Kassel -173-> Frankfurt -217-> Würzburg -186-> Erfurt",
                    "München",   "Kassel -502-> München",
                    "Mannheim",  "Kassel -173-> Frankfurt -85-> Mannheim"
            ),
            "Würzburg",
            Map.of(
                    "Nürnberg",  "Würzburg -103-> Nürnberg",
                    "Frankfurt", "Würzburg -217-> Frankfurt",
                    "Würzburg",  "Würzburg",
                    "Augsburg",  "Würzburg -103-> Nürnberg -167-> München -84-> Augsburg",
                    "Kassel",    "Würzburg -217-> Frankfurt -173-> Kassel",
                    "Stuttgart", "Würzburg -103-> Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "Würzburg -217-> Frankfurt -85-> Mannheim -80-> Karlsruhe",
                    "Erfurt",    "Würzburg -186-> Erfurt",
                    "München",   "Würzburg -103-> Nürnberg -167-> München",
                    "Mannheim",  "Würzburg -217-> Frankfurt -85-> Mannheim"
            ),
            "Stuttgart",
            Map.of(
                    "Nürnberg",  "Stuttgart -183-> Nürnberg",
                    "Frankfurt", "Stuttgart -183-> Nürnberg -103-> Würzburg -217-> Frankfurt",
                    "Würzburg",  "Stuttgart -183-> Nürnberg -103-> Würzburg",
                    "Augsburg",  "Stuttgart -183-> Nürnberg -167-> München -84-> Augsburg",
                    "Kassel",    "Stuttgart -183-> Nürnberg -103-> Würzburg -217-> Frankfurt -173-> Kassel",
                    "Stuttgart", "Stuttgart",
                    "Karlsruhe", "Stuttgart -183-> Nürnberg -103-> Würzburg -217-> Frankfurt -85-> Mannheim -80-> Karlsruhe",
                    "Erfurt",    "Stuttgart -183-> Nürnberg -103-> Würzburg -186-> Erfurt",
                    "München",   "Stuttgart -183-> Nürnberg -167-> München",
                    "Mannheim",  "Stuttgart -183-> Nürnberg -103-> Würzburg -217-> Frankfurt -85-> Mannheim"
            ),
            "Mannheim",
            Map.of(
                    "Nürnberg",  "Mannheim -85-> Frankfurt -217-> Würzburg -103-> Nürnberg",
                    "Frankfurt", "Mannheim -85-> Frankfurt",
                    "Augsburg",  "Mannheim -80-> Karlsruhe -250-> Augsburg",
                    "Würzburg",  "Mannheim -85-> Frankfurt -217-> Würzburg",
                    "Kassel",    "Mannheim -85-> Frankfurt -173-> Kassel",
                    "Stuttgart", "Mannheim -85-> Frankfurt -217-> Würzburg -103-> Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "Mannheim -80-> Karlsruhe",
                    "Erfurt",    "Mannheim -85-> Frankfurt -217-> Würzburg -186-> Erfurt",
                    "München",   "Mannheim -80-> Karlsruhe -250-> Augsburg -84-> München",
                    "Mannheim",  "Mannheim"
            ),
            "Frankfurt",
            Map.of(
                    "Nürnberg",  "Frankfurt -217-> Würzburg -103-> Nürnberg",
                    "Frankfurt", "Frankfurt",
                    "Würzburg",  "Frankfurt -217-> Würzburg",
                    "Augsburg",  "Frankfurt -85-> Mannheim -80-> Karlsruhe -250-> Augsburg",
                    "Kassel",    "Frankfurt -173-> Kassel",
                    "Stuttgart", "Frankfurt -217-> Würzburg -103-> Nürnberg -183-> Stuttgart",
                    "Karlsruhe", "Frankfurt -85-> Mannheim -80-> Karlsruhe",
                    "Erfurt",    "Frankfurt -217-> Würzburg -186-> Erfurt",
                    "München",   "Frankfurt -217-> Würzburg -103-> Nürnberg -167-> München",
                    "Mannheim",  "Frankfurt -85-> Mannheim"
            )
    );

    @Override
    protected Stream<? extends Arguments> provideDefaultArguments(ExtensionContext context) {
        return PATHS.entrySet().stream().map(entry -> Arguments.of(entry.getKey(), entry.getValue()));
    }
}
