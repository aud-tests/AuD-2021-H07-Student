package h07.util.provider;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

import static h07.util.Config.*;
import static h07.util.TestClass.getMethodSignature;

/**
 * A class representing argument sources. <br><br>
 * Allows the injection of custom arguments if used as an argument source.
 * Must be extended by all argument providers.
 */
abstract public class Provider implements ArgumentsProvider {

    /**
     * Returns the concatenation of injected and default argument streams and limits the stream size
     * to the number specified in {@link h07.util.Config#NUMBER_OF_TEST_RUNS}.
     * @inheritDoc
     */
    @Override
    public final Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        String testMethod = context.getRequiredTestClass().getTypeName() + '#' + getMethodSignature(context.getRequiredTestMethod());

        return Stream.concat(INJECTED_ARGUMENTS.getOrDefault(testMethod, Stream.of()), provideDefaultArguments(context))
                     .limit(NUMBER_OF_TEST_RUNS.getOrDefault(testMethod, DEFAULT_NUMBER_OF_TEST_RUNS));
    }

    /**
     * Provides a stream of arguments for {@link Provider#provideArguments(ExtensionContext)}
     * @param context the current extension context; never {@code null}
     * @return a stream of arguments
     * @see h07.util.Config#INJECTED_ARGUMENTS
     */
    abstract protected Stream<? extends Arguments> provideDefaultArguments(ExtensionContext context);
}
