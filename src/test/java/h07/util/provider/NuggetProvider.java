package h07.util.provider;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

import static h07.util.TestClass.RANDOM;

public class NuggetProvider extends Provider { // I wish...

    /** The correct combination of package numbers for 0 to 99 nuggets */
    private static final int[][] PACKAGE_NUMBERS = {
            { 0, 0, 0},       null,       null,       null,       null,       null, { 1, 0, 0},       null,       null, { 0, 1, 0},
                  null,       null, { 2, 0, 0},       null,       null, { 1, 1, 0},       null,       null, { 3, 0, 0},       null,
            { 0, 0, 1}, { 2, 1, 0},       null,       null, { 4, 0, 0},       null, { 1, 0, 1}, { 3, 1, 0},       null, { 0, 1, 1},
            { 5, 0, 0},       null, { 2, 0, 1}, { 4, 1, 0},       null, { 1, 1, 1}, { 6, 0, 0},       null, { 3, 0, 1}, { 5, 1, 0},
            { 0, 0, 2}, { 2, 1, 1}, { 7, 0, 0},       null, { 4, 0, 1}, { 6, 1, 0}, { 1, 0, 2}, { 3, 1, 1}, { 8, 0, 0}, { 0, 1, 2},
            { 5, 0, 1}, { 7, 1, 0}, { 2, 0, 2}, { 4, 1, 1}, { 9, 0, 0}, { 1, 1, 2}, { 6, 0, 1}, { 8, 1, 0}, { 3, 0, 2}, { 5, 1, 1},
            {10, 0, 0}, { 2, 1, 2}, { 7, 0, 1}, { 9, 1, 0}, { 4, 0, 2}, { 6, 1, 1}, {11, 0, 0}, { 3, 1, 2}, { 8, 0, 1}, {10, 1, 0},
            { 5, 0, 2}, { 7, 1, 1}, {12, 0, 0}, { 4, 1, 2}, { 9, 0, 1}, {11, 1, 0}, { 6, 0, 2}, { 8, 1, 1}, {13, 0, 0}, { 5, 1, 2},
            {10, 0, 1}, {12, 1, 0}, { 7, 0, 2}, { 9, 1, 1}, {14, 0, 0}, { 6, 1, 2}, {11, 0, 1}, {13, 1, 0}, { 8, 0, 2}, {10, 1, 1},
            {15, 0, 0}, { 7, 1, 2}, {12, 0, 1}, {14, 1, 0}, { 9, 0, 2}, {11, 1, 1}, {16, 0, 0}, { 8, 1, 2}, {13, 0, 1}, {15, 1, 0}
    };

    @Override
    protected Stream<? extends Arguments> provideDefaultArguments(ExtensionContext context) {
        return RANDOM.ints(0, 100).distinct().mapToObj(i -> Arguments.of(i, PACKAGE_NUMBERS[i]));
    }
}
