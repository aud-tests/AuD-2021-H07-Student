package h07.util.provider;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

import static h07.util.TestClass.RANDOM;

public class PathProvider extends Provider {

    @Override
    protected Stream<? extends Arguments> provideDefaultArguments(ExtensionContext context) {
        return Stream.generate(() -> {
            int n = RANDOM.nextInt(5) + 1;
            Character[] nodes = RANDOM.ints(0x41, 0x5B)
                                      .distinct()
                                      .limit(n)
                                      .mapToObj(i -> (char) i)
                                      .toArray(Character[]::new);
            int[] arcWeights = RANDOM.ints(n - 1, 0, 1000).toArray();

            return Arguments.of(nodes, arcWeights);
        });
    }
}
