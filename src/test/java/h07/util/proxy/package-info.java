/**
 * Contains classes that act as proxies for interfaces.
 * @see java.lang.reflect.Proxy
 */
package h07.util.proxy;
