package h07.experiment;

import h07.util.TestClass;
import h07.util.provider.NuggetProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.lang.reflect.Method;

import static h07.util.Assertions.*;
import static java.lang.reflect.Modifier.*;

/**
 * Tests for class {@code ChickenNuggets}.
 */
public class ChickenNuggetsTest extends TestClass {

    /** {@code Method} object for {@code ChickenNuggets.computePackageNumbers(int)} */
    public final Method computePackageNumbers;

    /**
     * Creates a new {@link ChickenNuggetsTest} object. Requires that {@code ChickenNuggets} exists.
     */
    public ChickenNuggetsTest() {
        super("h07.experiment.ChickenNuggets", null);

        computePackageNumbers = getMethodByName("computePackageNumbers(int)");
    }

    /**
     * Tests the definition of {@code ChickenNuggets}.
     * <ul>
     *     <li>Asserts that the class is public</li>
     *     <li>Asserts that {@code computePackageNumbers(int)} is
     *     public and static and has return type {@code int[]}</li>
     * </ul>
     */
    @Test
    @Override
    public void testDefinition() {
        // class
        assertHasModifiers(classDescriptor, PUBLIC);

        // methods
        assertHasModifiers(computePackageNumbers, PUBLIC, STATIC);
        assertReturnType(computePackageNumbers, int[].class.getTypeName());
    }

    /**
     * Tests for {@code ChickenNuggets.computePackageNumbers(int)}. <br>
     * Asserts that the tested method returns an array with the same values as expected.
     * @throws ReflectiveOperationException if any invocation fails
     */
    @ParameterizedTest
    @ArgumentsSource(NuggetProvider.class)
    public void testComputePackageNumbers(int numberOfNuggets, int[] expectedPackageNumbers) throws ReflectiveOperationException {
        int[] actualPackageNumbers = (int[]) computePackageNumbers.invoke(null, numberOfNuggets);

        assertArrayEquals(expectedPackageNumbers, actualPackageNumbers, "Returned array of package numbers did not equal expected one");
    }
}
