package h07.experiment;

import h07.TemplateTests;
import h07.util.TestClass;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

import static h07.util.Assertions.*;
import static java.lang.reflect.Modifier.*;

/**
 * Tests for class {@code ChickenNuggetGraphFactory}. <br><br>
 * Depends on {@link TemplateTests.DirectedGraphFactoryTemplateTest}.
 */
public class ChickenNuggetGraphFactoryTest extends TestClass {

    /** {@code Method} object for {@link h07.graph.DirectedGraphFactory#createDirectedGraph()} */
    public final Method createDirectedGraph;

    /** Instance of {@link ChickenNuggetGraphFactoryTest} */
    private final ChickenNuggetGraphFactoryTest testClassInstance = this;

    /**
     * Creates a new {@link ChickenNuggetGraphFactoryTest} object. Requires that
     * {@code ChickenNuggetGraphFactory} exists and has a constructor with no parameters.
     */
    public ChickenNuggetGraphFactoryTest() {
        super("h07.experiment.ChickenNuggetGraphFactory", constructor -> constructor.getParameterCount() == 0);

        TemplateTests.DirectedGraphFactoryTemplateTest directedGraphFactory = TemplateTests.INSTANCE.new DirectedGraphFactoryTemplateTest();

        createDirectedGraph = directedGraphFactory.createDirectedGraph;
    }

    /**
     * Tests the definition of {@code ChickenNuggetGraphFactory}.
     * <ul>
     *     <li>Asserts that the class...<ul style="margin-top: 0; margin-bottom: 0">
     *         <li>is public</li>
     *         <li>is not abstract</li>
     *         <li>is not generic</li>
     *         <li>implements {@link h07.graph.DirectedGraphFactory}</li>
     *     </ul></li>
     * </ul>
     */
    @Test
    @Override
    public void testDefinition() {
        assertHasModifiers(classDescriptor, PUBLIC);
        assertDoesNotHaveModifiers(classDescriptor, ABSTRACT);
        assertNotGeneric(classDescriptor);
        assertImplements(classDescriptor, "h07.graph.DirectedGraphFactory<java.lang.Integer, java.lang.Integer>");
    }

    /**
     * Tests for the {@code DirectedGraph} returned by {@code ChickenNuggetGraphFactory.createDirectedGraph()}. <br><br>
     * Depends on {@link TemplateTests.DirectedGraphTemplateTest}.
     */
    @Nested
    @SuppressWarnings("unchecked")
    public class ChickenNuggetGraphTest {

        /** {@code Method} object for {@code h07.graph.DirectedGraph.getAllNodes()} */
        public final Method getAllNodes;
        /** {@code Method} object for {@code h07.graph.DirectedGraph.getChildrenForNode(V)} */
        public final Method getChildrenForNode;
        /** {@code Method} object for {@code h07.graph.DirectedGraph.getArcWeightBetween(V, V)} */
        public final Method getArcWeightBetween;
        /** {@code Method} object for {@code h07.graph.DirectedGraph.addNode(V)} */
        public final Method addNode;
        /** {@code Method} object for {@code h07.graph.DirectedGraph.removeNode(V)} */
        public final Method removeNode;
        /** {@code Method} object for {@code h07.graph.DirectedGraph.connectNodes(V, A, V)} */
        public final Method connectNodes;
        /** {@code Method} object for {@code h07.graph.DirectedGraph.disconnectNodes(V, V)} */
        public final Method disconnectNodes;

        /** {@link Collection} of expected nodes */
        private final Collection<Integer> nodes = Set.of(0, 1, 2, 3, 4, 5);

        /**
         * Creates a new {@link ChickenNuggetGraphFactoryTest.ChickenNuggetGraphTest} object.
         */
        public ChickenNuggetGraphTest() {
            TemplateTests.DirectedGraphTemplateTest directedGraphTemplateTest = TemplateTests.INSTANCE.new DirectedGraphTemplateTest();

            getAllNodes = directedGraphTemplateTest.getAllNodes;
            getChildrenForNode = directedGraphTemplateTest.getChildrenForNode;
            getArcWeightBetween = directedGraphTemplateTest.getArcWeightBetween;
            addNode = directedGraphTemplateTest.addNode;
            removeNode = directedGraphTemplateTest.removeNode;
            connectNodes = directedGraphTemplateTest.connectNodes;
            disconnectNodes = directedGraphTemplateTest.disconnectNodes;
        }

        /**
         * Tests for {@code getAllNodes()} on the {@code DirectedGraph} returned by
         * {@code ChickenNuggetGraphFactory.createDirectedGraph()}. <br>
         * Asserts that the collection returned by the tested method is immutable and has the same size and elements as {@code nodes}.
         * @throws ReflectiveOperationException if any invocation fails
         */
        @Test
        public void testGetAllNodes() throws ReflectiveOperationException {
            Collection<Integer> actualNodes = (Collection<Integer>) getAllNodes.invoke(newInstance());

            // collection has correct size
            assertEquals(nodes.size(), actualNodes.size(), "Number of nodes in the returned collection differs from expected amount");
            // collection has all nodes
            assertTrue(actualNodes.containsAll(nodes), "The returned collection does not contain all nodes");

            // unmodifiable Collection
            assertThrows(UnsupportedOperationException.class, () -> actualNodes.add(0), "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, () -> actualNodes.addAll(Set.of(0)), "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, actualNodes::clear, "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, () -> actualNodes.remove(0), "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, () -> actualNodes.removeAll(Set.of(0)), "Returned collection is not immutable");
            assertThrows(UnsupportedOperationException.class, () -> actualNodes.retainAll(Set.of(0)), "Returned collection is not immutable");
        }

        /**
         * Tests for {@code getChildrenForNode(Integer)} on the {@code DirectedGraph} returned by
         * {@code ChickenNuggetGraphFactory.createDirectedGraph()}. <br>
         * Asserts that the collection returned by the tested method is immutable. Further asserts that the collection
         * has the same size and elements as the expected set of children for a given node. Also asserts that the method
         * throws a {@link NullPointerException} if invoked with {@code null} and a {@link NoSuchElementException} if
         * invoked with a node that is not in this graph.
         * @throws ReflectiveOperationException if any invocation fails
         */
        @Test
        public void testGetChildrenForNode() throws ReflectiveOperationException {
            Object instance = newInstance();

            for (Integer node : nodes) {
                Collection<Integer> children = (Collection<Integer>) getChildrenForNode.invoke(instance, node);

                // collection has correct size
                assertEquals(2, children.size(), "Number of child nodes in the returned collection differs from expected amount");
                // collection has all child nodes
                assertTrue(children.containsAll(Set.of((node + 2) % 6, (node + 3) % 6)),
                        "The returned collection does not contain all child nodes");

                // unmodifiable Collection
                assertThrows(UnsupportedOperationException.class, () -> children.add(0), "Returned collection is not immutable");
                assertThrows(UnsupportedOperationException.class, () -> children.addAll(Set.of(0)), "Returned collection is not immutable");
                assertThrows(UnsupportedOperationException.class, children::clear, "Returned collection is not immutable");
                assertThrows(UnsupportedOperationException.class, () -> children.remove(0), "Returned collection is not immutable");
                assertThrows(UnsupportedOperationException.class, () -> children.removeAll(Set.of(0)), "Returned collection is not immutable");
                assertThrows(UnsupportedOperationException.class, () -> children.retainAll(Set.of(0)), "Returned collection is not immutable");
            }

            // node == null
            assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, getChildrenForNode, (Object) null),
                    "Method did not throw a NullPointerException when invoked with null");

            // node is not in graph
            assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getChildrenForNode, 6),
                    "Method did not throw a NoSuchElementException when invoked with node that is not in graph");
        }

        /**
         * Tests for {@code getArcWeightBetween(Integer, Integer)} on the {@code DirectedGraph} returned by
         * {@code ChickenNuggetGraphFactory.createDirectedGraph()}. <br>
         * Asserts that the values returned by the tested method for given pairs of nodes equals the expected ones.
         * Also asserts that the method throws a {@link NullPointerException} if invoked with {@code null} and a
         * {@link NoSuchElementException} if invoked with a node that is not in this graph.
         * @throws ReflectiveOperationException if any invocation fails
         */
        @Test
        public void testGetArcWeightBetween() throws ReflectiveOperationException {
            Object instance = newInstance();

            for (Integer node : nodes) {
                // node is connected to the one two places ahead and has correct weight
                assertEquals(20, getArcWeightBetween.invoke(instance, node, (node + 2) % 6),
                        "Method did return the correct value for the arc weight between the nodes " + node + " and " + ((node + 2) % 6));
                // node is connected to the one three places ahead and has correct weight
                assertEquals(9, getArcWeightBetween.invoke(instance, node, (node + 3) % 6),
                        "Method did return the correct value for the arc weight between the nodes " + node + " and " + ((node + 3) % 6));
            }

            // node == null
            assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, getArcWeightBetween, null, 0),
                    "Method did not throw a NullPointerException when invoked with null as first parameter");
            assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, getArcWeightBetween, 0, null),
                    "Method did not throw a NullPointerException when invoked with null as second parameter");
            assertThrows(NullPointerException.class, () -> invokeExpectingException(instance, getArcWeightBetween, null, null),
                    "Method did not throw a NullPointerException when invoked with null as first and second parameter");

            // node is not in graph
            assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getArcWeightBetween, -1, 0),
                    "Method did not throw a NoSuchElementException when invoked with non-existing node as first parameter");
            assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getArcWeightBetween, 0, 6),
                    "Method did not throw a NoSuchElementException when invoked with non-existing node as second parameter");
            assertThrows(NoSuchElementException.class, () -> invokeExpectingException(instance, getArcWeightBetween, -1, 6),
                    "Method did not throw a NoSuchElementException when invoked with non-existing node as first and second parameter");
        }

        /**
         * Tests for {@code addNode(Integer)} on the {@code DirectedGraph} returned by
         * {@code ChickenNuggetGraphFactory.createDirectedGraph()}. <br>
         * Asserts that a {@link UnsupportedOperationException} is thrown whenever invoked, regardless of parameters.
         */
        @Test
        public void testAddNode() {
            Object instance = newInstance();

            // node = distinct
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, addNode, 6),
                    "Method did not throw an UnsupportedOperationException when invoked with new node");

            // node = duplicate
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, addNode, 0),
                    "Method did not throw an UnsupportedOperationException when invoked with duplicate node");

            // node = null
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, addNode, (Object) null),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
        }

        /**
         * Tests for {@code removeNode(Integer)} on the {@code DirectedGraph} returned by
         * {@code ChickenNuggetGraphFactory.createDirectedGraph()}. <br>
         * Asserts that a {@link UnsupportedOperationException} is thrown whenever invoked, regardless of parameters.
         */
        @Test
        public void testRemoveNode() {
            Object instance = newInstance();

            // node = distinct
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, removeNode, 6),
                    "Method did not throw an UnsupportedOperationException when invoked with non-existent node");

            // node = duplicate
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, removeNode, 0),
                    "Method did not throw an UnsupportedOperationException when invoked with existing node");

            // node = null
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, removeNode, (Object) null),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
        }

        /**
         * Tests for {@code connectNodes(Integer, Integer, Integer)} on the {@code DirectedGraph} returned by
         * {@code ChickenNuggetGraphFactory.createDirectedGraph()}. <br>
         * Asserts that a {@link UnsupportedOperationException} is thrown whenever invoked, regardless of parameters.
         */
        @Test
        public void testConnectNodes() {
            Object instance = newInstance();

            // node = distinct
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, 0, 0, 1),
                    "Method did not throw an UnsupportedOperationException when invoked with unconnected nodes");

            // node = duplicate
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, 0, 0, 2),
                    "Method did not throw an UnsupportedOperationException when invoked with already connected nodes");

            // node = null
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, null, 0, 0),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, 0, null, 0),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, 0, 0, null),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, null, null, 0),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, null, 0, null),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, 0, null, null),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, connectNodes, null, null, null),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
        }

        /**
         * Tests for {@code disconnectNodes(Integer, Integer)} on the {@code DirectedGraph} returned by
         * {@code ChickenNuggetGraphFactory.createDirectedGraph()}. <br>
         * Asserts that a {@link UnsupportedOperationException} is thrown whenever invoked, regardless of parameters.
         */
        @Test
        public void testDisconnectNodes() {
            Object instance = newInstance();

            // node = distinct
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, disconnectNodes, 0, 2),
                    "Method did not throw an UnsupportedOperationException when invoked with connected nodes");

            // node = duplicate
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, disconnectNodes, 0, 1),
                    "Method did not throw an UnsupportedOperationException when invoked with already disconnected nodes");

            // node = null
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, disconnectNodes, 0, null),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, disconnectNodes, null, 0),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
            assertThrows(UnsupportedOperationException.class, () -> invokeExpectingException(instance, disconnectNodes, null, null),
                    "Method did not throw an UnsupportedOperationException when invoked with null");
        }

        /**
         * Returns a new {@code DirectedGraph} object. This method invokes
         * {@code ChickenNuggetGraphFactory.createDirectedGraph()} and returns the result.
         * @return the {@code DirectedGraph} instance
         */
        public Object newInstance() {
            try {
                return createDirectedGraph.invoke(testClassInstance.newInstance());
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException("An exception occurred when invoking the constructor of " + className, e);
            }
        }
    }
}
