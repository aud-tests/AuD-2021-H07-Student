package h07;

import h07.util.TestClass;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;

import static h07.util.Assertions.*;
import static java.lang.reflect.Modifier.*;

/**
 * Tests for the template's interfaces. <br><br>
 * These tests assert that the important interfaces and methods (i.e., those that are in the
 * template) haven't been modified to prevent consequential mistakes because all subsequent
 * implementations would be wrong also. Because of this, these tests should always pass or else
 * something has gone seriously wrong.
 */
public class TemplateTests {

    /** {@link TemplateTests} object to use instead of instantiating the class over and over. */
    @SuppressWarnings("InstantiationOfUtilityClass")
    public static final TemplateTests INSTANCE = new TemplateTests();

    /**
     * Tests for interface {@code Monoid}.
     */
    @Nested
    public class MonoidTemplateTest extends TestClass {

        /** {@code Method} object for {@code Monoid.zero()} */
        public final Method zero;
        /** {@code Method} object for {@code Monoid.add(M, M)} */
        public final Method add;

        /**
         * Creates a new {@link MonoidTemplateTest} object. Requires that {@code Monoid} exists.
         */
        public MonoidTemplateTest() {
            super("h07.algebra.Monoid", null);

            zero = getMethodByName("zero()");
            add = getMethodByName("add(M, M)");
        }

        /**
         * Tests the definition of {@code Monoid}.
         * <ul>
         *     <li>Asserts that the interface...<ul style="margin-top: 0; margin-bottom: 0">
         *         <li>is public</li>
         *         <li>is generic and has a type parameter M</li>
         *     </ul></li>
         *     <li>Asserts that {@code zero()} is public and abstract and has return type M</li>
         *     <li>Asserts that {@code add(M, M)} is public and abstract and has return type M</li>
         * </ul>
         */
        @Test
        @Override
        public void testDefinition() {
            // interface
            assertHasModifiers(classDescriptor, PUBLIC, INTERFACE);
            assertIsGeneric(classDescriptor, TypeParameter.of("M", UNBOUNDED));

            // methods
            assertHasModifiers(zero, PUBLIC, ABSTRACT);
            assertReturnType(zero, "M");

            assertHasModifiers(add, PUBLIC, ABSTRACT);
            assertReturnType(add, "M");
        }
    }

    /**
     * Tests for interface {@code ShortestPathsAlgorithm}.
     */
    @Nested
    public class ShortestPathsAlgorithmTemplateTest extends TestClass {

        /** {@code Method} object for {@code ShortestPathsAlgorithm.shortestPaths(DirectedGraph, V, Monoid, Comparator)} */
        public final Method shortestPaths;

        /**
         * Creates a new {@link ShortestPathsAlgorithmTemplateTest} object.
         * Requires that {@code ShortestPathsAlgorithm} exists.
         */
        public ShortestPathsAlgorithmTemplateTest() {
            super("h07.algorithm.ShortestPathsAlgorithm", null);

            shortestPaths = getMethodByName(
                    "shortestPaths(h07.graph.DirectedGraph<V, A>, V, h07.algebra.Monoid<A>, java.util.Comparator<? super A>)");
        }

        /**
         * Tests the definition of {@code ShortestPathsAlgorithm}.
         * <ul>
         *     <li>Asserts that the interface...<ul style="margin-top: 0; margin-bottom: 0">
         *         <li>is public</li>
         *         <li>is generic and has two type parameters, V and A</li>
         *     </ul></li>
         *     <li>Asserts that {@code shortestPaths(DirectedGraph, V, Monoid, Comparator)} is public
         *     and abstract and has return type {@code Map&lt;V, Path&lt;V, A&gt;&gt;}</li>
         * </ul>
         */
        @Test
        @Override
        public void testDefinition() {
            // interface
            assertHasModifiers(classDescriptor, PUBLIC, INTERFACE);
            assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);

            // methods
            assertHasModifiers(shortestPaths, PUBLIC, ABSTRACT);
            assertReturnType(shortestPaths, "java.util.Map<V, h07.graph.Path<V, A>>");
        }
    }

    /**
     * Tests for class {@code AbstractPath}.
     */
    @Nested
    public class AbstractPathTemplateTest extends TestClass {

        /**
         * Creates a new {@link AbstractPathTemplateTest} object. Requires that {@code AbstractPath} exists.
         */
        public AbstractPathTemplateTest() {
            super("h07.graph.AbstractPath", null);
        }

        /**
         * Tests the definition of {@code AbstractPath}.
         * <ul>
         *     <li>Asserts that the class...<ul style="margin-top: 0; margin-bottom: 0">
         *         <li>is abstract</li>
         *         <li>is package-private</li>
         *         <li>is generic and has two type parameters, V and A</li>
         *         <li>implements {@code Path}</li>
         *     </ul></li>
         * </ul>
         */
        @Test
        @Override
        public void testDefinition() {
            // class
            assertHasModifiers(classDescriptor, ABSTRACT);
            assertDoesNotHaveModifiers(classDescriptor, PUBLIC, PROTECTED, PRIVATE);
            assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);
            assertImplements(classDescriptor, "h07.graph.Path<V, A>");
        }
    }

    /**
     * Tests for interface {@code DirectedGraph}.
     */
    @Nested
    public class DirectedGraphTemplateTest extends TestClass {

        /** {@code Method} object for {@code DirectedGraph.getAllNodes()} */
        public final Method getAllNodes;
        /** {@code Method} object for {@code DirectedGraph.getChildrenForNode(V)} */
        public final Method getChildrenForNode;
        /** {@code Method} object for {@code DirectedGraph.getArcWeightBetween(V, V)} */
        public final Method getArcWeightBetween;
        /** {@code Method} object for {@code DirectedGraph.addNode(V)} */
        public final Method addNode;
        /** {@code Method} object for {@code DirectedGraph.removeNode(V)} */
        public final Method removeNode;
        /** {@code Method} object for {@code DirectedGraph.connectNodes(V, A, V)} */
        public final Method connectNodes;
        /** {@code Method} object for {@code DirectedGraph.disconnectNodes(V, V)} */
        public final Method disconnectNodes;

        /**
         * Creates a new {@link DirectedGraphTemplateTest} object. Requires that {@code DirectedGraph} exists.
         */
        public DirectedGraphTemplateTest() {
            super("h07.graph.DirectedGraph", null);

            getAllNodes = getMethodByName("getAllNodes()");
            getChildrenForNode = getMethodByName("getChildrenForNode(V)");
            getArcWeightBetween = getMethodByName("getArcWeightBetween(V, V)");
            addNode = getMethodByName("addNode(V)");
            removeNode = getMethodByName("removeNode(V)");
            connectNodes = getMethodByName("connectNodes(V, A, V)");
            disconnectNodes = getMethodByName("disconnectNodes(V, V)");
        }

        /**
         * Tests the definition of {@code DirectedGraph}.
         * <ul>
         *     <li>Asserts that the interface...<ul style="margin-top: 0; margin-bottom: 0">
         *         <li>is public</li>
         *         <li>is generic and has two type parameters, V and A</li>
         *     </ul></li>
         *     <li>Asserts that all methods are public and abstract and have the correct return type</li>
         * </ul>
         */
        @Test
        @Override
        public void testDefinition() {
            // interface
            assertHasModifiers(classDescriptor, PUBLIC, INTERFACE);
            assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);

            // methods
            assertHasModifiers(getAllNodes, PUBLIC, ABSTRACT);
            assertReturnType(getAllNodes, "java.util.Collection<V>");

            assertHasModifiers(getChildrenForNode, PUBLIC, ABSTRACT);
            assertReturnType(getChildrenForNode, "java.util.Collection<V>");

            assertHasModifiers(getArcWeightBetween, PUBLIC, ABSTRACT);
            assertReturnType(getArcWeightBetween, "A");

            assertHasModifiers(addNode, PUBLIC, ABSTRACT);
            assertReturnType(addNode, void.class.getTypeName());

            assertHasModifiers(removeNode, PUBLIC, ABSTRACT);
            assertReturnType(removeNode, void.class.getTypeName());

            assertHasModifiers(connectNodes, PUBLIC, ABSTRACT);
            assertReturnType(connectNodes, void.class.getTypeName());

            assertHasModifiers(disconnectNodes, PUBLIC, ABSTRACT);
            assertReturnType(disconnectNodes, void.class.getTypeName());
        }
    }

    /**
     * Tests for interface {@code DirectedGraphFactory}.
     */
    @Nested
    public class DirectedGraphFactoryTemplateTest extends TestClass {

        /** {@code Method} object for {@code DirectedGraphFactory.createDirectedGraph()} */
        public final Method createDirectedGraph;

        /**
         * Creates a new {@link DirectedGraphFactoryTemplateTest} object.
         * Requires that {@code DirectedGraphFactory} exists.
         */
        public DirectedGraphFactoryTemplateTest() {
            super("h07.graph.DirectedGraphFactory", null);

            createDirectedGraph = getMethodByName("createDirectedGraph()");
        }

        /**
         * Tests the definition of {@code DirectedGraphFactory}.
         * <ul>
         *     <li>Asserts that the interface...<ul style="margin-top: 0; margin-bottom: 0">
         *         <li>is public</li>
         *         <li>is generic and has two type parameters, V and A</li>
         *     </ul></li>
         *     <li>Asserts that {@code zero()} is public and abstract and
         *     has return type {@code DirectedGraph&lt;V, A&gt;}</li>
         * </ul>
         */
        @Test
        @Override
        public void testDefinition() {
            // interface
            assertHasModifiers(classDescriptor, PUBLIC, INTERFACE);
            assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);

            // methods
            assertHasModifiers(createDirectedGraph, PUBLIC, ABSTRACT);
            assertReturnType(createDirectedGraph, "h07.graph.DirectedGraph<V, A>");
        }
    }

    /**
     * Tests for interface {@code Path}.
     */
    @Nested
    public class PathTemplateTest extends TestClass {

        /** {@code Method} object for {@code Path.traverser()} */
        public final Method traverser;
        /** {@code Method} object for {@code Path.concat(V, A)} */
        public final Method concat;
        /** {@code Method} object for {@code Path.iterator()} */
        public final Method iterator;
        /** {@code Method} object for {@code Path.of(V)} */
        public final Method of1;
        /** {@code Method} object for {@code Path.of(V, A, V)} */
        public final Method of2;
        /** {@code Method} object for {@code Path.of(V, A, V, A, V)} */
        public final Method of3;

        /**
         * Creates a new {@link PathTemplateTest} object. Requires that {@code Path} exists.
         */
        public PathTemplateTest() {
            super("h07.graph.Path", null);

            traverser = getMethodByName("traverser()");
            concat = getMethodByName("concat(V, A)");
            of1 = getMethodByName("of(V)");
            of2 = getMethodByName("of(V, A, V)");
            of3 = getMethodByName("of(V, A, V, A, V)");

            Method iteratorTmp;

            try {
                iteratorTmp = Iterable.class.getDeclaredMethod("iterator");
            } catch (NoSuchMethodException e) {
                iteratorTmp = null;
                e.printStackTrace();
            }

            iterator = iteratorTmp;
        }

        /**
         * Tests the definition of {@code Path}.
         * <ul>
         *     <li>Asserts that the interface...<ul style="margin-top: 0; margin-bottom: 0">
         *         <li>is public</li>
         *         <li>is generic and has two type parameters, V and A</li>
         *         <li>extends {@link Iterable}</li>
         *     </ul></li>
         *     <li>Asserts that the methods are all public and abstract and have the correct return type</li>
         * </ul>
         */
        @Test
        @Override
        public void testDefinition() {
            // interface
            assertHasModifiers(classDescriptor, PUBLIC, INTERFACE);
            assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);
            assertImplements(classDescriptor, "java.lang.Iterable<V>");

            // methods
            assertHasModifiers(traverser, PUBLIC, ABSTRACT);
            assertReturnType(traverser, "h07.graph.Path$Traverser<V, A>");

            assertHasModifiers(concat, PUBLIC, ABSTRACT);
            assertReturnType(concat, "h07.graph.Path<V, A>");

            assertHasModifiers(iterator, PUBLIC, ABSTRACT);
            assertReturnType(iterator, "java.util.Iterator<T>");

            assertHasModifiers(of1, PUBLIC, STATIC);
            assertReturnType(of1, "h07.graph.Path<V, A>");

            assertHasModifiers(of2, PUBLIC, STATIC);
            assertReturnType(of2, "h07.graph.Path<V, A>");

            assertHasModifiers(of3, PUBLIC, STATIC);
            assertReturnType(of3, "h07.graph.Path<V, A>");
        }
    }

    /**
     * Tests for interface {@code Path.Traverser}.
     */
    @Nested
    public class TraverserTemplateTest extends TestClass {

        /** {@code Method} object for {@code Path.Traverser.getCurrentNode()} */
        public final Method getCurrentNode;
        /** {@code Method} object for {@code Path.Traverser.getDistanceToNextNode()} */
        public final Method getDistanceToNextNode;
        /** {@code Method} object for {@code Path.Traverser.walkToNextNode()} */
        public final Method walkToNextNode;
        /** {@code Method} object for {@code Path.Traverser.hasNextNode()} */
        public final Method hasNextNode;

        /**
         * Creates a new {@link TraverserTemplateTest} object. Requires that {@code Path.Traverser} exists.
         */
        public TraverserTemplateTest() {
            super("h07.graph.Path$Traverser", null);

            getCurrentNode = getMethodByName("getCurrentNode()");
            getDistanceToNextNode = getMethodByName("getDistanceToNextNode()");
            walkToNextNode = getMethodByName("walkToNextNode()");
            hasNextNode = getMethodByName("hasNextNode()");
        }

        /**
         * Tests the definition of {@code Path.Traverser}.
         * <ul>
         *     <li>Asserts that the interface...<ul style="margin-top: 0; margin-bottom: 0">
         *         <li>is public</li>
         *         <li>is generic and has two type parameters, V and A</li>
         *     </ul></li>
         *     <li>Asserts that the declared methods are public and abstract and have the correct return type</li>
         * </ul>
         */
        @Test
        @Override
        public void testDefinition() {
            // interface
            assertHasModifiers(classDescriptor, PUBLIC, INTERFACE);
            assertIsGeneric(classDescriptor, TYPE_PARAMETERS_VA);

            // methods
            assertHasModifiers(getCurrentNode, PUBLIC, ABSTRACT);
            assertReturnType(getCurrentNode, "V");

            assertHasModifiers(getDistanceToNextNode, PUBLIC, ABSTRACT);
            assertReturnType(getDistanceToNextNode, "A");

            assertHasModifiers(walkToNextNode, PUBLIC, ABSTRACT);
            assertReturnType(walkToNextNode, void.class.getTypeName());

            assertHasModifiers(hasNextNode, PUBLIC, ABSTRACT);
            assertReturnType(hasNextNode, boolean.class.getTypeName());
        }
    }
}
