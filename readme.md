# Community Tests für die siebte Hausübung der AuD 2021


Zum Ausführen der Tests sollte eine [eigene JUnit Run Configuration](https://git.rwth-aachen.de/groups/aud-tests/-/wikis/JUnit-Run-Configuration) angelegt werden, da durch den gradle task nicht alle Meldungen angezeigt werden (z.B. warum Tests ignoriert werden).

Einige Methoden müssen intern nach bestimmten Anforderungen aufgebaut sein. Mit Black-box testing (worunter diese Tests fallen) kann man dabei allerdings nur bedingt Aussagen über die Korrektheit treffen, weil die Ergebnisse identisch sein können. \
Die Tests können also unter Umständen nur prüfen, ob die Definitionen von Klassen, Attributen und Methoden sowie deren Werte bzw. Rückgaben korrekt sind. Da Attribute bzw. Methoden nicht immer mit Namen benannt sind, müssen sie manchmal nach Kriterien bestimmt werden, wodurch es zu einer `NoSuchFieldException` bzw. `NoSuchMethodException` (auch für Konstruktoren) kommen kann. Details, z.B. wo der Fehler aufgetreten ist, finden sich im Stacktrace und im jeweiligen Prädikat.

Einige Aspekte der Tests können mithilfe der Datei [`Config.java`](src/test/java/h07/util/Config.java) angepasst werden. Mehr Infos finden sich in der Datei selbst.

Die Tests kommen mit einem Installer sowie einem Update-Mechanismus, der vor jeder Ausführung nach Updates sucht. Da das einige Sekunden in Anspruch nehmen kann und vielleicht auch anderweitig nicht gewünscht ist, kann diese Funktionalität ausgeschaltet werden. Wird der Installer oder der Update-Mechanismus verwendet, muss das Arbeitsverzeichnis gleich dem Projektordner sein. Das Verhalten kann mit Änderung der Konstanten `CHECK_FOR_UPDATES`, `CHECK_HASHES` und `AUTO_UPDATE` in der Konfiguration verändert werden. \
Vielen Dank an @svenja.kernig für die Idee und Hilfe mit XML :D

Die Dokumentation kann [hier](https://aud-tests.pages.rwth-aachen.de/AuD-2021-H07-Student/index.html) eingesehen werden.

DISCLAIMER: Das Durchlaufen der Tests ist keine Garantie dafür, dass die Aufgaben vollständig korrekt implementiert sind.
